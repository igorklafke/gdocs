# Ações

As ações vinculadas à uma atividade do fluxo podem ser utilizadas para executar uma transição de fluxo, ou remover o usuário logado da lista de reponsáveis atuais pelo item.

Apenas o responsável pela atividade pode visualizar as ações. Se uma ação for vinculada à um [evento temporal](evento-temporal.md) ela não aparecerá para os usuários.



## Ação para transição de fluxo

Comportamento padrão das ações.

## Ação para remover responsabilidade do usuário

É uma ação que tem como função somente retirar o usuário que a executa da lista de responsáveis atuais. 

O tipo de ação pode ser alterado clicando-se no link "Regras" ao lado do nome da ação, na lista de ações da atividade.

**Exemplo de aplicação:** 

    Em uma atividade onde vários usuários precisam comentar um mesmo documento, mas apenas um deles pode tramitar o documento no fluxo.
    Neste caso a atividade poderá ter uma ação deste tipo para os usuários que só precisem comentar o documento, e uma outra ação para o usuário que irá tramitar o documento no fluxo quando necessário.

>ref. [PBI 4536](https://w3k.visualstudio.com/DefaultCollection/GreenDocs%20Git/_workitems?_a=edit&id=4536 "Product Backlog Item 4536: Novo tipo de ação em atividades BPM")