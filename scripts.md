# Manual Script's:

Será documentado aqui como utilizar os comandos e funções disponíveis no script do Greendocs.


## Campos do Item/Documento:

ID do Documento: item.ID  
Nome Documento: item.Name  
Número Revisão: item.RevisionNumber  
Número Revisão Original: item.OriginalNumber  
Número Sub Revisão: item.SubRevisionNumber  
Sigla Tipo de Item: item.Type


## Metadados do Item/Documento:

| TIPO DE CAMPO                     | SINTÁXE                                                                                                                                                                            | EXEMPLO                                                                                                                                                     |
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Multi Valorado (array de string): | Leitura:  Item.Fields.nome_campo[posicao]  Escrita:  Item.Fields.nome_campo = [“opcao1”, “opcao2”];                                                                                | Leitura:  var teste =,item.Fields.Informacoes[0];   Escrita:  item.Fields.Informacoes = ["Ruim", "Péssimo"];                                                |
| Tabela:                           | Leitura:  item.Fields.campo_tabela.Rows[índice_linha].Columns["nome_coluna"];   Escrita:  item.Fields.campo_tabela.Rows[índice_linha].Columns["nome_coluna"] = 666;                | Leitura:  var t = item.Fields.Informacoes.Rows[0].Columns["Usuarios_Leitura"];  Escrita: item.Fields.Informacoes.Rows[0].Columns["Usuarios_Leitura"] = 666; |
| Lista (valores fixos):            | Leitura:  item.Fields.nome_campo;   Escrita:  item.Fields.nome_campo = "opcao";                                                                                                    | Leitura:  var teste = item.Fields.Modalidade_de_Licenciamento;   Escrita:  item.Fields.Modalidade_de_Licenciamento = "Concorrente";                         |
| Multi Valorado em tabela:         | Leitura:  item.Fields.campo_tabela.Rows[índice_linha].Columns["nome_coluna"][índice];   Escrita:  item.Fields.campo_tabela.Rows[índice_linha].Columns["nome_coluna"] = ["opcao1"]; | Leitura: var teste =,item.Fields.Informacoes.Rows[0].Columns["Multi"][0];   Escrita: item.Fields.Informacoes.Rows[0].Columns["Multi"] = ["Talvez"];         |
| Lista sigla (valores fixos):      | Leitura:  item.Fields.nome_campo;   Escrita:  item.Fields.nome_campo = "sigla - opcao";                                                                                            | Leitura: var teste = item.Fields.Prioridade;   Escrita: item.Fields.Prioridade = "2 - Média";                                                               |


## Campos de Usuário:

Objeto: User

ID do Usuário: User.ID  
Login: User.Login  
Nome: User.Name  
Posições: User.Positions

### Exemplo de sintáxe:
```js
item.Fields.Especialista = User.ID;
```


## Campos de Posição:

Area (área)  
	- ID (id)  
	- Name (nome)  
	- Initials (sigla)  
Division (unidade)  
	- ID (id)  
	- Name (nome)  
	- Initials (sigla)  
Role (função)  
	- ID (id)  
	- Name (nome)  
	- Initials (sigla)

### Exemplo de sintáxe:
```js
if(User.Positions[0].Area.ID > 0) {
	var idArea = User.Positions[0].Area.ID;
}
```


## Função de Log:


### Exemplo de sintáxe:
```js
log(“teste ” + User.ID);
```


## Função de Queima de Sequencial:


### Exemplo de sintáxe:
```js
setSequence(chave, sequencial, chaveReserva, sequenciaMaximaReserva);
```


## Função que busca Usuário pelo ID:


### Exemplo de sintáxe:
```js
var usuario = getUserByID(item.Fields.Especialista);
```


## Função para criar nova pasta:


### Exemplo de sintáxe:
```js
var idPasta = newFolder(idPastaPai, nomePasta, siglaPasta = null);
```


## Função para buscar valor interno do metadado:


### Exemplo de sintáxe:
```js
var idFieldValue = getFieldValueID(item, nomeInterno);
```


## Função para verificar se já existe um item com o mesmo nome:


### Exemplo de sintáxe:
```js
var existe = itemExists(“Contrato1”);
```


## Função para criar um alerta na tela do usuário via SignalR:


### Exemplo de sintáxe:
```js
showMessage(“Documento já existe!”);
```