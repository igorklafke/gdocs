Notas da Versão
====

Neste documento serão inseridos todas as informações relevandes sobre alterações realizadas em cada versão.
A ordem apresentada aqui é decrescente, ou seja, as versões mais atuais aparecem no topo da página.

**As principais pontos, que precisam estar neste documento são:**

- Itens relevantes de backlog incluídos na versão
- Alterações de comportamentos do funcionalidades já existentes
- Alterações de configuração/procedimentos necessárias em alterações de funcionalidades

## Versão 6.8

- Sistema portado para banco de dados Oracle 12c. Requer release específico que deve ser solicitado junto a W3K caso haja a necessidade.

## Versão 6.7

### Alterações que requerem intervenção no momento da atualização

- Versão mínima do .Net Framework necessária: 4.5
- Devido a modificação na estrutura do banco de dados, será necessário atualizar manualmente as configurações de turnos nos ambientes após atualização para esta versão.
- Opção de utilizar pesquisa Fulltext. Para informações adicionais verificar a seção adequada em [Performance](Performance.md).

### Funcionalidades adicionadas na versão

- Configuração que permite bloquear tela de dados do usuário para alteração por ele próprio.
- Sequencial semiautomático. Configuração em tipo de item que permite que o sequencial seja criado automaticamente mas que possa ser alterado
em itens de LD ou página do item, sem interferir no controle de sequencia já gerado.
- Opção de um nível adicional de sub-revisão. Agora utilizando o revisionamento composto e sub-revisão em conjunto,
há três níveis de revisão permitidos.
- Opção para que ao incrementar revisão, os arquivos da revisão anterior não constem na revisão nova.
- No resultado da pesquisa, incluiu-se a possibilidade de utilizar os filtros, assim como hoje acontece na visualizações.
- Na tela de comentários de documentos, campo para pesquisa foi incluído,
que instantaneamente busca pelo termo digitado dentre os comentários apresentados na tela.
- Pastas podem ser excluídas, mesmo que com referências. A pasta é marcada como excluída mas fica no banco de dados para fins de histórico.
- Exportação de ambiente em ZIP pode ser particionado, para ambientes grandes. O particionamento divide os dados em vários arquivos ZIP.
- Melhoria de velocidade e amplitude da pesquisa em ambiente ECM (pesquisa por metadados e por conteúdo) através da utilização de
recurso de FullText Search do MS-SQL. Agora a pesquisa é executada de forma mais rápida e em todos os campos do sistema e não apenas
em nome e pasta. Para mais detalhes sobre ambientes ECM verificar a documentação em [Performance](Performance.md).  
  


## Versão 6.6

- Arquivos em formulários foram incluídos nos resultados das pesquisas por conteúdo
- Opção de novo controle para campos lista que permite o auto completar se um filho é selecionado em uma cascata o pai é automaticamente preenchido. 
- Pesquisa avançada: pesquisa com seleção de campos a serem pesquisados e critérios de pesquisa nestes campos
	- Opção de salvar os critérios da pesquisa avançada
	- Opção de alterar os critérios utilizados na pesquisa após encontrar os primeiros resultados
- Nova opção de fluxo de cancelamento de revisão
- Ao selecionar uma revisão de um documento, agora pode-se selecionar uma data.
Ao selecionar a data é apresentada a revisão ativa na data selecionada.
- Inclusão de um ou mais arquivos de template em cada tipo de item.
- Tradução de labels de campos. Os labels de campos podem conviver simultaneamente em português, inglês e espanhol.