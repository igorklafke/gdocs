# Revisões

O GreenDocs permite o controle de revisões de documentos e formulários. O tipo de revisionamento utilizado deve ser configurado no ambiente, na aba Geral -> Revisionamento.

Ao selecionar o tipo de revisionamento pode-se visualizar uma pequena descrição de como ele funciona, cada uma delas pode ter diferentes opções que serão apresentadas de acordo com o valor selecionado.

Quatro tipos de revisionamento são possíveis:

- **Numérica:** Revisão numérica simples, incrementando 1. Utilize o campo "Iniciar em" para definir o valor da primeira revisão.
- **Alfabética:** Revisão do documento inicia em A e cada incremento sobe uma letra. É possí­vel configurar para que algumas letras sejam ignoradas, é comum em alguns projetos pularem as letras I e O, por exemplo. Para isso, adicione as letras que devem ser ignoradas no campo "Ignorar Caracteres" separadas por ponto e vírgula (;).
- **Composta:** É composta pela revisão cheia e revisão parcial. Acontece quando várias revisÃµes parciais alfabéticas formam uma revisão cheia numérica. Ex: 0A, 0B, 0C, 0, 1A, 1B, 1, ... No processo deverá ser configurado quando que a revisão cheia ou parcial será incrementada. Assim como na revisão alfabética, é possível ignorar caracteres que serÃ£o utilizados na segunda parte (letras) do número da revisão.
- **Alfanumérica:** Revisão do documento incia em 0 e incremeta numéricamente até que passe a ser incrementada por letras. Ex: 0, 1, 2, 3, A, B, C, ... No processo deverá ser configurado quando que a revisão é alterada de numérica para alfabética.

Além destas definições, é necessário configurar no processo/fluxo onde e como serão criadas as revisões dos itens.

## Gerando uma nova revisão para um item

A revisão de um item pode ser incrementada de duas maneiras, por um evento no fluxo do item ou no momento do upload de um novo arquivo para um documento.

### Incrementar a revisão no fluxo

Para que o incremento da revisão de um item se dê numa transição de fluxo, é necessário adicionar um [evento](http://www.greendocs.ne) do tipo "Revisão" na configuração no fluxo do processo associado ao tipo de item.
Após adicionar este evento no fluxo, pode-se clicar duas vezes sobre o mesmo para ver suas configurações avançadas. Se o ambiente estiver configurado para utilizar revisão composta, deve-se selecionar também se a revisão cheia ou parcial deverá ser incrementada.

A opção "Gerar sub revisão" irá criar uma subrevisão para o item atual, esta sub-revisão será finalizada no próximo evento de revisão que não tenha essa opção também marcada pelo qual o item passar.

As configurações padrão de Campos e Ações não se aplicam neste evento.


### Incrementar a revisão no upload de um arquivo


## Edição manual do número da revisão

Para que a revisão de um documento possa ser alterada manualmente por um usuário, a configuração do ambiente deve permitir e o usuário deve ter a permissão apropriada.

Na configuração do ambiente, aba Geral -> Revisionamento, marcar a opção "Permitir alteração manual da revisão". E, na aba Permissões -> Usuários, adicionar os usuários desejados na permissão "Permitir alteração manual da revisão".

Ao habilitar esta configuração, uma nova opção fica disponível, "Obrigatório Motivo da Troca de Revisão", que irá obrigar o usuário a inserir um motivo para a alteração manual do número da revisão. Quando marcada, um campo para adicionar uma lista de motivos possíveis é exibido, estes motivos são opcionais.

## Cancelar revisão

É possível habilitar o cancelamento da revisão atual na configuração de uma Atividade. Na aba "Revisão", marcando a opção "Habilitar cancelamento da revisão". Quando esta opção estiver marcada, o responsável pela atividade terá um botão "Cancelar Revisão Atual" no canto inferior direito na página do item. Só é possível cancelar a revisão se ela não for a primeira revisão do item.

>ref. [PBI 4559](https://w3k.visualstudio.com/DefaultCollection/GreenDocs%20Git/_workitems?_a=edit&id=4559 "Product Backlog Item 4559: Cancelamento da revisao deve ser feito apenas pelo responsavel da atividade")

## Sub-revisão