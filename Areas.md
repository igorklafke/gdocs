| Área                                | Desenvolvedor |
|-------------------------------------|---------------|
| Arquivos                            | Gustavo       |
| Comentários                         | Gustavo       |
| Compartilhamento de itens           | Lucas         |
| Configuração de posições            | Gustavo       |
| Configuração do ambiente            | Gustavo       |
| Controle de Horas                   | Lucas         |
| Exclusão de itens                   | Gustavo       |
| Exportação                          | Igor          |
| Exportar/Importar ambientes         | Gustavo       |
| Fluxo                               | Igor          |
| Geração de código                   | Gustavo       |
| Integrações - Via API               | Lucas         |
| Integrações - WebService            | Igor          |
| Locais (Pastas)                     | Gustavo       |
| Metadados                           | Lucas         |
| Notificações                        | Igor          |
| Pesquisa                            | Gustavo       |
| Referência entre itens              | Lucas         |
| Relatórios - Configuráveis          | Lucas         |
| Relatórios - Customizados           | Gustavo       |
| Relatórios - Gráficos Configuráveis | Lucas         |
| Revisionamento                      | Lucas         |
| Tipo de item/Cronograma             | Igor          |
| Visualizações                       | Gustavo       |
| Visualizador - Autovue              | Gustavo       |
| WinApp Cache Local                  | Lucas         |
| Widgets                             | Lucas         |
| Visualizador - QC                   | Igor          |
| Medição                             | Igor          |