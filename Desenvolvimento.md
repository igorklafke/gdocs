#Informações para desenvolvedores

##Ambiente de desenvolvimento

###Montando um ambiente do zero para desenvolvimento

Passo a passo para preparar uma máquina para começar a desenvolver. Esse procedimento considera um ambiente novo, com Windows recém instalado.

1. Instalar Visual Studio Community 2013: 
[http://www.visualstudio.com/products/visual-studio-community-vs](http://www.visualstudio.com/products/visual-studio-community-vs) ou pelo ISO no \\\desenv\\temp\sw. Nas funcionalidades opcionais a serem instaladas, deixar marcado apenas "Microsoft SQL Server Data Tools" e "Microsoft Web Developer Tools"
2. Baixar e instalar o [Report Viewer 2010 Redistributable Package](http://www.microsoft.com/en-us/download/details.aspx?id=6442)
3. Iniciar o Visual Studio e logar com o usuário e senha do Visual Studio Online
4. Quando abrir um projeto pela primeira vez, antes de compilar acessar Tools -> NuGet Package Manager -> Manage NuGet Packages for Solution... Na tela que abrir clicar em "Restore"

###Geração de pacotes para deploy

O GreenDocs é distribuído através de pacotes de publicação web, a geração deste pacote poderá ser feita no servidor W3KDBSRV21, utilizando o usuário build. 
O script para geração deste pacote é encontrado na pasta c:\source\GreenDocs_Git\\_build, e deve ser executado com os parâmetros do nome do branch e do profile de publicação. Conforme abaixo: 

		.\build.ps1 [branch] [profile]
		
Os profiles possíveis são: cloud, premises ou parceiro.

Exemplo para gerar um pacote para produção:

		.\build.ps1 master cloud
		
Exemplo para gerar um pacote da EXE:

		.\build.ps1 exe premises
		
Após conclusão do script o pacote estará disponível no BitSync, dentro da pasta respectiva do profile e da versão gerada.

##Convenções do código

###Areas

O ASP.Net MVC possui uma divisão por *Areas*, no GreenDocs há uma área Configuracao, onde deverão ficar todos os Controllers e Views relacionados à interface de configuração. Tudo que for de interface de usuário fica na raiz do GreenDoc.Web.

Como a *area* de configuração foi criada posteriormente, a maioria do que já existe hoje nesta interface ainda não está na *Area* correta. Isso deve ser migrado sob demanda.

###Data Services

Um arquivo [NomeEntidade]Service.cs para cada entidade principal do sistema, ex: ProjetoService.cs, DocumentoService.cs, etc.

Ainda há um legado onde códigos de determinadas entidades estão em outros Services, como por exemplo Metadado e TipoDeItem dentro do Projeto. Isso deve ser alterado sob demanda.

###Scripts

Os arquivos .js em GreenDoc.Web são separados em duas pastas principais: greendocs e vendor.

- Pasta **greendocs:** Arquivos javascript desenvolvidos pela W3K. Cada "visão" principal do GreenDocs possui um arquivo .js correspondente, uma visão é um arquivo .aspx, que pode carregar várias views parciais (ascx). O código javascript destas views parciais estará também no arquivo js da view principal.
 
 
- Pasta **vendor:** arquivos javascript de plugins de terceiros. jQuery, etc.

Links e botões devem ter um id que iniciam com link ou button, como *link_[nomeAcao]* e *button_[nomeAcao]*. Para o caso de uma lista, seja de links, botões ou campos, classes css para definir ações iniciando com action, *action_[nomeAcao]*. Os parâmetros necessários devem ser definidos usando atributos data-, conforme exemplo.

HTML:

	<div class="action_abrirAgrupamento" data-idgrup="<%:item.ID_Agrupamento %>">
		<%=item.Descricao%>
	</div>

JavaScript:

	$(".action_abrirAgrupamento").click(function () {
		var id = $(this).data('idgrup');
	});




##Procedimentos para desenvolvimento

###Utilizando o Code First

Ver [CodeFirst](CodeFirst.md)

###Restaurar banco de dados

Exemplo de script SQL para restaurar backup:
	
	RESTORE DATABASE [GreenDocs_br01_Dev] 
	FROM  DISK = N'T:\Backups DBs\BR01\greendoc.bak' 
	WITH  FILE = 1,  MOVE N'GreenDoc' 
	TO N'D:\db\greendocs_br01_dev\GreenDocs_br01_Dev.mdf',  
	MOVE N'GreenDoc_log' 
	TO N'D:\db\greendocs_br01_dev\GreenDocs_br01_Dev.ldf',  
	NOUNLOAD,  REPLACE,  STATS = 5
	GO
	
Exemplo de script SQL para alterar todas as senhas dos usuários para greendoc e remover e-mails de notificação:

	UPDATE Usuarios SET EmailNotificacao = NULL
	UPDATE Usuarios SET Pass = '011563b3e0ba74df225b59529ba4232a0189e1ed67554d3733f3a59b14cd24fb25d3ae6347e492e5602b72891dd117da88fa70413a039311613cee7dcce15987'

Exemplo de script SQL para alterar licença para modo desenvolvedor (br01):

	UPDATE ControleLicencas SET 
	DataValidade='636818976000000000',
	Chave='KTKJ3-NWAJF-OV5LA-MIBIW-V0BCE', 
	CodigoAtivacao='sddvnhf', 
	ID_Registro='548968DA-5F64-40BB-9EA0-3A85D57F04F0',
	DataAtivacao='635362272000000000'
	GO

###Checklist para Code Review

1. **Testes unitários:** Verificar se os testes unitários cobrem o código acrescentado.
2. **Parâmetros dos métodos:** Fazem sentido e são validados corretamente.
3. **Null references:** Verificar se não há possibilidade de erros no futuro por um objeto ser nulo.
4. **Try Catch:** Não deve ser usado para "esconder" erros.
4. **Consistência com as convenções:** Estrutura do código e nomes devem estar de acordo com as convenções.
5. **Refatorações sob demanda:** Mudanças que envolvem interface de configuração devem ter sido migradas para a Area correspondente. Assim como alterações no Data Services. Conforme convenções.
6. **Segurança:** Verificar se não existe a possibilidade de usuários visualizarem o que não devem, se um recurso pode ser acessado sem o usuário estar autenticado, etc.