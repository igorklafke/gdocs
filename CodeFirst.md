Utilização do Code First no GreenDocs
=====================================

Instruções básicas para desenvolvimento no GreenDocs após alteração para utilização do Entity Framework Code First.

Arquitetura
-----------

Para suportar as alterações necessárias para a implementação do Code First algumas mudanças foram necessárias na arquitetura. Primeiramente o GreenDoc.DAL deixa de existir e a camada de acesso aos dados será o GreenDoc.Data.
A lógica do repository também muda, estes agora são simplesmente classes genéricas para acesso ao banco, e as regras de negócio que anteriormente ficavam no repositor agora ficam nos services (DocumentoService, ProjetoService, etc...). Conforme esquema abaixo:

Procedimentos
-------------

A partir desta alteração não será mais necessário criar tabelas e colunas no banco de dados para então atualizar o model, sendo realizado o caminho inverso, as modificações poderão ser feitas diretamente no model que são automaticamente efetuadas no banco de dados através de um comando.
Como desta maneira o EF sabe em que “versão” está o banco, o deploy para produção e instalações em clientes é facilitado, já que o banco de dados pode ser atualizado automaticamente após a instalação do GreenDocs.

### Alterando o model

As alterações são efetuadas diretamente no arquivo com a classe do modelo, que se encontram em GreenDoc.Data\Model.

#### Criar nova coluna
Para se criar uma nova propriedade (coluna) simplesmente adiciona-se esta propriedade à classe, por exemplo:

	public string NovaColuna { get; set; }

#### Criar relacionamento entre duas classes
Para se criar uma referência entre dois objetos utiliza-se uma propriedade virtual, por exemplo, para criar um relacionamento entre os objetos Documento e Usuário, adiciona-se ao documento:

	public virtual Usuario UsuarioDoDocumento { get; set; }
E, no usuário:

	public virtual Documento DocumentoDoUsuario { get; set; }

Se quisesse fazer um relacionamento múltiplo, como um usuário ter vários documentos, por exemplo, utilizaria um ICollection, assim a propriedade no usuário ficaria:

	public virtual ICollection<Documento> DocumentosDoUsuario { get; set; }

Note que não é necessário criar explicitamente as colunas para o relacionamento, como um ID_Usuario na classe Documento, por exemplo para o caso acima, o entity framework irá criar automaticamente as colunas no banco de dados. Mas, se necessário, é possível fazer o mapeamento da seguinte manerira:

	public long ID_DocumentoUsuario { get; set; }

	[ForeignKey("ID_DocumentoUsuario")]
	public virtual Documento DocumentoDoUsuario { get; set; }

#### Criar uma nova classe modelo

Para criar um novo modelo (tabela), basta adicionar a classe na pasta models com as propriedades desejadas, conforme exemplos acima. A classe deve ser ‘public partial class’ e para definir chaves primárias e se as colunas aceitam nulos ou não são utilizadas *DataAnnotatios*, por exemplo:

	public partial class NovoModel
	{
		[Key]
		public long ID_NovoModel { get; set; }
		[Required]
		public string Descricao { get; set; }
	}

E adicionar um DbSet à classe GreenDocContext:

	public DbSet<NovoModel> NovosModels { get; set; }

Para adicionar uma tabela com múltiplas chaves primárias, criar uma classe de mapeamento, exemplo:

 	public class NovoModeloMap : EntityTypeConfiguration<NovoModel>
    {
        public NovoModelMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Chave_1, t.Chave_2 });
        }
    }

E adicioná-la como configuração no GreenDocContext:

 	protected override void OnModelCreating(DbModelBuilder modelBuilder)
 	{
 		...
 		modelBuilder.Configurations.Add(new NovoModeloMap());
 	}


Outros exemplos de anotações podem ser vistos aqui: http://msdn.microsoft.com/en-us/data/gg193958.aspx

Especificamente para o GreenDocs é necessário também criar o repository para a nova entidade. Isto é feito declarando-se uma nova propriedade na classe UnitOfWork no GreenDoc.Data, conforme exemplo:

	private GenericRepository<NovoModel> novoModelRepository;

	public GenericRepository<NovoModel> NovosModel
	{
		get
		{
			if (this.novoModelRepository == null)
			{
				this.novoModelRepository = new GenericRepository<NovoModel>(context);
			}
			return novoModelRepository;
       }
	}

### Atualizando o banco

Após fazer as alterações nas classes do model, abrir o Package Manager Console (menu View -> Other Windows no Visual Studio), selecionar como Default Project o GreenDoc.Data e executar o seguinte comando:

	Add-Migration NomeMigration

Onde NomeMigration é um nome que é dado para a alteração feita. Não tem muita utilidade prática, mas é bom dar um nome que seja de acordo com a alteração que foi realizada. Já que este nome pode ser utilizado para atualizar um banco até esta migração, mesmo que outras alterações tenham sido feitas posteriormente, ou voltar o estado do banco para como estava antes desta migração.

Se necessário, é possível atualizar o código gerado ou incluir o método Seed para preencher alguma coluna com valores padrões.
Ao iniciar a aplicação, o banco de dados será atualizado automaticamente para refletir as alterações. Alternativamente, pode-se executar o comando para forçar a atualização o banco de dados:

	Update-Database

Este comando irá executar todas as migrations ainda não aplicadas ao banco de dados que estiver configurado na connection string do App.Config do GreenDocs.Data.
Mais informações: http://msdn.microsoft.com/en-US/data/jj591621
