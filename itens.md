# Itens

## Criação

    [em construção]

Para que um item possa ser criado, seu tipo de item precisa ter a oção "Permitir a criação de itens manualmente" marcada. Com esta opção os usuários que tiverem permissão no [processo] associado ao tipo de item irão ter o mesmo disponível para criação através do botão "Adicionar" na interface. Com esta opção desmarcada, o tipo de item só poderá ser criado por dentro de outro item onde as [regras de relacionamento] permitam que ele seja criado.

### Não permitir que um item seja criado se já existir outro item cadastrado com o mesmo valor de um campo

Para não permitir o preenchimento de um valor de um campo com o mesmo valor que já exista em outro item no ambiente, na configuração do campo, se este for do tipo "string" deve-se marcar a opção "Não permitir valor duplicado no mesmo ambiente".

**Exemplo de aplicação:** 

    No cadastro de um cliente, não permitir que dois clientes sejam cadastrados com o mesmo CNPJ. 

>ref. [PBI 4115](https://w3k.visualstudio.com/DefaultCollection/GreenDocs%20Git/_workitems?_a=edit&id=4115 "Product Backlog Item 4115: Não permitir duplicação de determinado campo em um form/documento")