#Setagens com impacto em performance

## Carregamento de documentos no mesmo nível de Local

Na navegação por Locais(pastas) não tenta carregar documentos se houverem sub-pastas.
Com esta configuração ativada só podem haver documentos no último nível de pastas.
Se houverem documentos no mesmo nível de uma pasta, estes documentos não serão carregados.

`insert into ConfigConstantes (ID_Projeto, Constante, ID_Valor) VALUES (0000, 'OCULTA_DOCUMENTOS_PASTAS', 1)`

PS: Substituir o '0000' do script pelo ID do ambiente que se deseja aplicar a configuração.  

## Setagem de ambiente ECM

- Ao apresentar documentos, seja via navegação de Locais(pastas) ou via pesquisa, considera apenas permissões por Locais, nenhuma outra.

`update projetos set Aplicacao='ECM' where ID_Projeto=0000`
 
PS: Substituir o '0000' do script pelo ID do ambiente que se deseja aplicar a configuração.

### Melhoria na velocidade da pesquisa com opção de Fulltext do MS-SQL

Só pode ser utilizada em ambientes setados como ECM e recurso deve estar instalado no servidor SQL.

O ambiente deve ser do tipo ECM e opção "FullText" deve estar como true na tabela Servidor. Script que deve ser executado no banco de dados:

	CREATE FULLTEXT CATALOG FTSearch
        CREATE FULLTEXT INDEX ON IndiceArquivos
        (Conteudo LANGUAGE 1046)
        KEY INDEX "PK_dbo.IndiceArquivos"
        ON FTSearch
        CREATE FULLTEXT INDEX ON IndiceDocumentos
        (Indice LANGUAGE 1046)
        KEY INDEX "PK_dbo.IndiceDocumentos"
        ON FTSearch