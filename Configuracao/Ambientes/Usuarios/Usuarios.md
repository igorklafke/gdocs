#Configuração de Ambientes - Usuários
Possibilita o cadastro de posições de usuário, unidades, áreas e funções
##Posições
Permite que sejam criadas e filtradas posições para os usuários do ambiente.

Para criar uma posição deve-se clicar em um usuário da lista. Ao lado da lista de usuários será aberta a tela para cadastro de posição. Uma posição deve conter um dos três campos preenchidos, ou todos preenchidos. Um usuários poder ter nenhuma ou várias posições.

As posições criadas serão utilizadas em outras partes da configuração do Greendocs no que diz respeito a permissões e responsabilidades para filtrar os usuários.
##Unidades
Permite o cadastro de unidades, com sigla e nome. As unidades podem ser listadas como campos do tipo lista de unidades.
##Áreas
Permite o cadastro de áreas, com sigla e nome. As áreas podem ser listadas como campos do tipo lista de áreas.
##Funções
Permite o cadastro de funções, com sigla e nome. As funções podem ser listadas como campos do tipo lista de funções.