# Guia de configuração - Campos
Diretrizes do manual
 
•         Explicar o que é.
•         Explicar como se faz se aplicável
•         Apresentar a abrangência se aplicável
•         Incluir impactos se aplicável.

Campos ou metadados são elementos a serem preenchidos em documentos ou formulários. São definidos a partir das necessidades do projeto e do que deve ser preenchido ao manipular determinado item.

##Básicos

O Greendocs, por ser uma ferramenta configurável, permite ao usuário de serviço, a criação de campos ou conjunto de campos segundo a necessidade de cada tipo de item. O conjunto de campos pode ser criado, afim de, facilitar a inclusão dos campos necessários em tipos de itens diferentes. A partir da criação do conjunto, é possível agrupar os metadados necessários.

###Criação - Conjuntos

Na aba de configuração, em Básicas, o usuário deverá selecionar a opção "Novo Conjunto". Será exibido uma pop-up, para que o nome do conjunto seja informado.
A organização de criação de novos conjuntos fica a critério do administrador do sistema, porém recomendamos a criação de um conjunto com os metadados comuns em todos os tipos de itens e a criação de outros conjuntos com os metadados exclusivos de cada tipo de item. Na mesma tela, há um checkbox "Exibir nome de tela no formulário" que este estando habilitado exibem o nome do conjunto na tela de formulários.
###Criação - Propriedade Customizada

A criação de campos pode ser realizada pela "Nova Propriedade Customizada". Nessa tela, são exibidas as opções para a criação do metadado desejado. 
O usuário deverá incluir o Nome, selecionar o conjunto onde o metadado ficará agrupado, o tipo de metadado, o tamanho se aplicável, a largura do campo se aplicável, alinhamento do campo (horizontal - Ao lado do nome do metadado; vertical - Abaixo do metadado), além de um campo descrição (com o preenchimento desse campo, um ícone de ajuda será exibido ao lado do metadado criado em um formulário ou documento com o texto adicionado nessa caixa de texto). Ainda há dois checkbox que podem ser marcados. "Manter Histórico de Alterações" irá gravar no histórico as alterações realizados no campo em questão. Já o checkbox "Mostrar Informações Adicionais do Valor Selecionado" aplica-se para campos do tipo Lista de Usuários ou Grupos e irá exibir as informações adicionais cadastradas nos usuários ou grupos.

![Cria Campos](/CriaCampos.png "Cria Campos")




#####Tipos de Metadados
- Arquivo: Metadado para inclusão de arquivos em documentos ou formulários. Caso o tipo de documento seja um Formulário, os arquivos adicionados serão exibidos no fim do formulário também, na aba arquivos.


- Calculado: Selecionado esse tipo de metadado é possível cadastrar uma fórmula (cálculo) a ser executado em um documento ou formulário. Após a seleção, será exibido na tela de criação do metadado uma caixa de texto para a inclusão da fórmula. Para adicionar metadados a essa fórmula, o administrador do sistema deve informar o nome interno do metadado no campo disponível para a fórmula. Por exemplo (*Valor_a + Valor_b*);

- Checkbox: O metadado criado será um checkbox.

- Data: O campo será do tipo data. 

- Data/Hora: Além da data, o usuário poderá informar a hora.

- Inteiro: O campo permitirá a inclusão somente de números inteiros.

- 