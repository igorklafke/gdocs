#Exibições

Exibições são telas de visualização de documentos e formulários, que podem conter personalizações referentes a colunas, agrupamentos, dashboard entre outras.

##Colunas

Colunas são os campos que foram criados na aba "Campos" ou os campos padrão, como por exemplo: Nome, Revisão e Atividade.

Para criar uma nova coluna é necessário clicar no link "Adicionar Coluna Customizada", escolher um nome que deve ser exibido na tela e selecionar um campo. 

A coluna padrão "Descrição" pode ser usada para apresentar uma lista de campos em apenas uma coluna. A configuração dos campos que serão apresentados é feita dentro do tipo de item.

##Agrupamentos

Agrupamentos são formas de organizar os dados apresentados por uma visualização, os agrupamentos podem ser compostos de campos criados na aba "Campos" ou os campos padrão.

A criação do agrupamento é feita através do link "Novo Agrupamento", onde deve ser preenchido o nome de tela do agrupamento. A opção "Agrupamento por Data" serve para criar agrupamento baseados em um campo data, como por exemplo o "Prazo", com esta configuração o agrupamento exibe os documento organizados de acordo com a diferença de dias em relação a data atual.

A configuração do agrupamento deve ser feita utilizando o link "Editar", localizado ao lado do agrupamento que deseja-se editar. Na edição, alem de haver a possibilidade de trocar o nome e alterar o "Agrupamento por Data", existe uma tabela, onde deve ser adicionado os níveis do agrupamento.

Os níveis de agrupamento são as ordens dos agrupamentos, por exemplo um agrupamento "Situação > Finalidade", ira agrupar os itens de acordo com a situação dos mesmos e ao clicar em uma das situações, os itens da mesma serão agrupados por finalidade, sendo situação nível 0 e finalidade nível 1.

A configuração dos níveis pode ser feita utilizando pasta (Locais), modelo(Checklists), propriedade (Campos), atividade, processo, tipo de item e pasta (Estrutura).

##Widgets

Os widgets são relatórios gráficos utilizados para apresentar informações em um formato de dashboard. 

A criação dos widgets é feita através do link "Novo Widgets", na tela de criação deve ser preenchido:

- Nome de tela (Título do gráfico)
- Visualização padrão, que será utilizada no momento de exibição dos dados
- Tipo de gráfico: Pizza, barras verticais, barras horizontais e barras segmentadas por prazo (utilizada para criar um gráfico com informações referentes aos prazos)
- Gráfico: Metadado (Campos) ou builtin (Campos padrões), aqui deve ser informado qual será a informação que formará os agrupamentos.

A aba filtros existe a opção de exibir apenas dados do usuário logado e/ou um filtro por tipo de item a ser visualizado.


##Visualizações

As visualizações são as telas onde serão apresentados os documentos e formulários.

A criação é feita através do link "Adicionar Visualização", onde deve ser informado o nome e o tipo da visualização. Os tipos podem ser:

- Lista de Itens: lista de documentos
- Lista de Processos: lista de formulários
- Controle de Itens: visualização de controle de documentos com matrizes
- Relatórios
- Controle de Processos: visualização de controle de formulários com matrizes
- Conjuntos: agrupamento de visualizações
- Widgets: dashboard

### Aba geral

Pode ser editado o nome da visualização e o conjunto a que ela pertence, além disso existem:

- Apenas itens sob responsabilidade do usuário logado
- Exibir revisões anteriores
- Mostrar somente 'Pendências de Treinamentos' (Evento Treinamento)
- Mostrar somente 'Documentos Compartilhados' (Evento Compartilhar)
- Exibir agrupamentos vazios (Exibe agrupamentos mesmo vazios)
- Exibir itens mais recentes primeiro

Obs: Nas visualizações de controle deve-se selecionar as informações que formarão a matriz, utilizando os campos linha e coluna.

### Aba Tipo Item

Devem ser escolhidos os tipos de item que fazem parte da visualização.

### Aba Regras de Visibilidade

Deve ser escolhido se será apresentado no menu "Ir Para" e as regras de visualização. As regras podem ser regra por posição do usuário, por usuário fixo ou por grupo do usuário.

### Aba Filtros Fixos

Deve ser escolhidos os filtros fixos da visualização, que serão aplicados sobre a visualização sem a possibilidade de alteração por parte do usuário.

### Aba Colunas

Deve ser escolhidos as colunas que serão exibidas. 

Para adicionar uma nova coluna de ser usado o link "Adicionar Coluna", escolher o campo e o percentual de largura do mesmo, quanto maior o percentual, maior será o espaço do campo na visualização.

As colunas disponibilizadas na adição são as padrões e as criadas na aba "Colunas" da "Exibição".

OBS: A visualização deve sempre contar com 100% do percentual, nunca mais e nem menos. 

### Aba Agrupamentos

Nesta aba podem ser escolhidos os agrupamentos que devem ser disponibilizados para o usuário. 

Para adicionar um novo agrupamento a visualização deve-se utilizar o link "Adicionar Agrupamento", os agrupamentos disponíveis são os padrões e os criados na aba "Agrupamentos" da "Exibição".

### Aba Filtros

Nesta aba pode ser selecionado os campos que o usuário terá permissão de utilizar como filtro na visualização
