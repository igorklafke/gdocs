#Permissões
Permissões é a área onde devem ser configuradas as permissões do ambiente.

##Grupos
Grupo é a aba das permissões do ambiente onde deve ser configurado os grupos de usuários contratante e participantes

O grupo contratante deve ser escolhido o grupo do cliente que contratou o ambiente, para que seu usuários possam ter acesso ao ambiente. 

Os grupos participante são outros grupos de usuários que podem possuir acesso ao ambiente.

##Usuários
Lista todas permissões possíveis para usuários no ambiente. Para adicionar um usuário a um determinada permissão deve-se clicar sobre a permissão, uma lista ao lado da lista de permissões será aberta com um link *Adicionar Usuários*

###Adicionar Usuários a Permissão
Para adicionar um usuário a uma permissão primeiramente deve-se selecionar um grupo. Ao selecionar o grupo os usuários desse grupo serão carregados na lista Usuário e assim um usuário pode ser selecionado.

###Administrar o projeto
Permite o usuário administrar o projeto podendo fazer alterações no mesmo através do link configurações exibido na tela do usuário enquanto logado como cliente.

###Alterar responsáveis
Permite o usuário alterar os responsáveis de um item editando como administrador.

###Auditar itens
Permite o usuário [auditar](Ambientes->Checklist) itens.

###Cancelar itens
Permite o usuário cancelar itens criados.

###Cancelar Revisão
Permite o usuário cancelar uma revisão retornando o item para revisão anterior.

###Criar item via checklist
Permite o usuário criar um itens utilizando o [checklist](Ambientes->Checklist) previamente criado.

###Criar pastas manualmente
Permite o usuário criar pastas enquanto logado como cliente.

###Definir responsabilidades de treinamento
Permite o usuário associar responsabilidades de treinamento para itens enquanto logado como cliente.

###Editar campo somente leitura
Permite o usuário alterar campos somente leitura de itens criados.

###Editar como administrador
Permite o usuário alterar os itens e executar ações de fluxo mesmo sem ser o responsável.

###Editar itens em lote
Permite o usuário alterar, em lote, itens criados.

###Editar sem ser responsável
Permite o usuário alterar os itens mesmo sem ser o responsável.

###Exportar Dados Em Lote
Permite o usuário criar rotinas de exportação de itens do sistema.

###Importar planilha
Permite o usuário importar planilhas, criando itens em lote.

###Importar planilha de arquivos
Permite o usuário importar planilhas de arquivos, atualizando os arquivos em lote.

###Mover itens
Permite o usuário mover os itens de uma pasta para a outra.

###Permite Editar Listas de Campos
Permite o usuário editar a lista de valores de campos do item.

###Pode executar qualquer ação de fluxo
Permite o usuário executar qualquer ação de qualquer item criado.

###Regerar código de item
Permite o usuário, ao alterar um campo, alterar o código do item já criado.