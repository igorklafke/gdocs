#Relatórios

Os relatórios são personalizados, possuem filtros fixos e de usuários, além da configuração ter a opção de escolher as colunas ou mesmo criar um relatório em forma de um gráfico.

Para criar um relatório deve-se utilizar o link "Novo Relatório Personalizado" e informar um nome e descrição para o mesmo.

Para editá-lo basta clicar sobre o relatório que deseja alterar.

##Cabeçalho

Na aba cabeçalho pode ser alterado o nome e a descrição dos relatórios.

##Filtros fixos

Os filtros fixos são feitos internamente nos relatórios sem a opção de alteração por parte do usuário final.

As opções de filtro são:

* Exibe Apenas Dados do Usuário Logado
* Tipo de Item: itens que serão exibidos no relatório
* Filtro Campos: filtro por campos criados
* Filtro Atividades: filtro por atividades do processo
* Tipo de Item Relacionado
* Tipo de Lista: onde pode ser escolhido revisões, instâncias ou treinamentos 

Caso escolha revisões

* Revisão:
	* Todas: todas as revisões
	* Apenas a última: apenas a última revisão do documento
	* Apenas a primeira: apenas a primeira revisão do documento
* Arquivo em Anexo: não, sim ou indiferente

Caso escolha instâncias

* Filtro instâncias:
	* Todas as instâncias
	* Somente instâncias ativas

Caso escolha Treinamentos

* Situação Treinamento:
	* Todos
	* Concluído
	* Pendente


##Filtros de usuário

São os filtros que serão liberados para o cliente selecionar no momento da geração do relatório

* Exibe Filtro Local
* Exibe Filtro Data de Criação/Atualização
* Exibe Filtro Última Revisão
* Exibe Filtro Atividade
* Exibe Filtro Tipo Item
* Exibe Filtro Situação
* Exibe Filtro Data Base Revisão
* Filtro Campos: campos criados

##Colunas

Colunas que o relatório apresentará, pode ser escolhido um nome de tela para o campo, a largura da coluna e qual o campo que representa aquela coluna.

As colunas podem ser:

* Campos: campos criados
* propriedades Built-in: propriedades padrões
* Campos de Item Relacionado: campos referentes aos itens relacionados dos itens do relatório, é utilizado para mostrar o valor de um campo de um item relacionado ao item da linha do relatório.

##Gráficos

Representações Gráficas de um relatório, o gráfico pode ser tanto por propriedades customizadas (Campos) como por propriedades Built-in (Padrões).

Os tipos de Gráficos disponíveis são:

* Pizza
* Barras Verticais
* Barras Horizontais
* Barras Segmentadas por prazo: as colunas do gráfico representam o prazo do item em relação a data atual. 