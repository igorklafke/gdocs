#Configuração do Ambiente

##Tipos de Item

Um item é a principal entidade do GreenDocs, podendo ser um documento, um formulário que roda em um fluxo BPM, entre outros. No GreenDocs, diversos tipos de item podem ser configurados, permitindo diferentes comportamentos com relação a sua classificação, codificação, permissão, etc.

Ao adicionar um novo tipo de item, além de um nome e uma sigla identificando este tipo, é necessário escolher uma natureza. Pense na natureza como uma subclasse do tipo de item, ela pode ser:
- **Documento:** O tipo mais comum em uma aplicação de GED ou ECM, tipos de item desta natureza estão centrados em seu(s) arquivo(s).
- **Formulário:** Como o nome já diz, um formulário com campos para ser executado em fluxo.
- **Protocolo:** Tipo de item que agrupa outros tipos de item, geralmente documentos, e é utilizado para o gerenciamento de guias de remessa de documentos (GRD/GR/GRE).
- **Objeto:** Assim como o protocolo, agrupa outros tipos de item, mas não possui funcionalidades específicas. Um exemplo de objeto seria o cadastro de uma Pessoa ou Empresa onde seriam cadastrados documentos da mesma.
- **Lista de Itens:** Também agrupa outros tipos de item, geralmente documentos, e é utilizado para o gerenciamento de listas de documentos (LD), permitindo importar e exportar arquivos XLS com a lista de documentos associados.
- **Cronograma:** Agrupa outros tipos de item (objetos) mas permitindo que estes sejam relacionados em uma hierarquia, cada item associado ao cronograma se comporta como uma tarefa, com controle de datas de início e fim, duração e predecessores.

As opções disponíveis na edição de um tipo de item podem variar de acordo com a natureza selecionada. Para este manual todas as opções serão detalhadas e terão a informação de para qual naturezas se aplicam.

###Geral

Além do nome, sigla e natureza já descritos anteriormente e que podem ser alterados a qualquer momento, na aba de configuração Geral do tipo de item, existem configurações que se aplicam para todos as naturezas de tipos de item e algumas que não se aplicam apenas para formulários.

Opções disponíveis para **todos os tipos de item**:
- **Permitir a criação de itens manualmente:** Com esta opção marcada os usuários que tiverem permissão no [processo] associado ao tipo de item irão ter o mesmo disponível para criação através do botão "Adicionar" na interface. Com esta opção desmarcada, o tipo de item só poderá ser criado por dentro de outro item em que as [regras de relacionamento] permitam que ele seja criado.
- **Permitir a atualização de itens manualmente:** Permite que, ao fazer o upload de um arquivo, se o documento já existir no GreenDocs ele seja atualizado. Utilizado em conjunto com o tipo de criação "em lote". Com esta opção ativa, o tipo de item irá aparecer no botão "Adicionar" na interface mesmo que a opção de criar manualmente esteja desmarcada, mas o usuário conseguirá apenas atulizar itens que já existem e não criar um novo.
- **Incluir nos resultados de pesquisa:** Faz com que itens deste tipo sejam exibidos nos resultados da pesquisa. Esta opção é marcada por padrão na criação do tipo de item.
- **Permitir Comentários:** Habilita a inclusão de comentários no item. Quando marcada, habilita outra opção **Permitir que usuários com acesso ao item incluam comentários**, que quando marcada permite que qualquer usuário com acesso de leitura ao item faça comentários, caso contrário apenas os responsáveis pela atividade atual do item podem comentar.
- **Habilitar Controle de Horas Trabalhadas:** Habilita o [controle de horas](avançado->controlehoras) no tipo de item.
- **Permite copiar itens:** Habilita a opção de copiar o item. Quando o usuário copia o item, um novo item é criado com os mesmos valores de campos e com os mesmos arquivos, porém um novo código é gerado para os itens copiados.

Opções disponíveis para **documento, cronograma, protocolo, objeto e lista de itens**:
- **Situação:** A [situação](campos->situação) inicial que a revisão do item receberá no momento de sua criação.
- **Criação:** Pode ser de dois tipo, simples ou em lote. A criação simples exibe uma página para preenchimento dos campos e upload do arquivo. Na criação em lote, o usuário faz o upload de diversos arquivos e uma tabela é exibida para preenchimento dos campos, com uma linha para cada arquivo enviado.
- **Não permitir dois itens com o mesmo nome:** Checa no momento da criação do item se já existe um item com o mesmo nome no GreenDocs, se já exisitir, não permite a criação. Esta opção é mais utilizada quando a [codificação do item] é manual ou quando o tipo de item estiver configurado para [utilizar o nome do arquivo como nome do item].
- **Exibir confirmação do código ao criar item:** No momento da criação do item, após o preenchimento dos campos, será exibido para o usuário o código que será gerado para o item para que ele confirme ou não a criação com aquele nome.
- **Não exibir revisão:** Oculta as informações de revisões do item.
- **Permitir compartilhamento:** Permite que o item seja compartilhado quando o item passar por um [evento de compartilhamento] no fluxo.
- **Validar nome do anexo comentário com nome do item:** Obriga que os arquivos anexos incluídos em comentários possuam o mesmo nome que o item no GreenDocs (a extensão do arquivo não precisa ser a mesma).
- **Pode ser cancelado:** Habilita a opção de cancelamento no item. Um item cancelado é como se o item estivesse excluído, mas continua visível em algumas visualizações.
- **Permitir Multiplos Locais:** Permite que múltiplos [locais](campos->locais) sejam associados ao item.
- **Permitir incluir em qualquer nível de local:** Permite que o item seja associado a um [local](campos->locais) em qualquer nível da estrutura de locais. Com esta opção desmarcada, o item só pode ser associado a um local que esteja no último nível na estrutura.
- **Pasta:** O [local](campos->locais) padrão para a criação do item. Quando o local for atribuído como propriedade do tipo de item, se o local selecionado nesta opção for a raíz da estrutura, o local deverá ser selecionado pelo usuário, caso contrário o local selecionado aqui será preenchido como local padrão do item.

Opções disponíveis apenas para **formulário**:
- **Usuários podem acessar instâncias sobre as quais executaram alguma ação:** Permite que, mesmo que o usuário não tenha permissão específica no processo, ele consiga continuar visualizando o formulário após o mesmo sair da atividade em que ele realizou alguma ação.
- **Permite Anexar Arquivos:** Para habilitar a inclusão de arquivos no item.

Opções disponíveis apenas para **cronograma**:
- **Data Início:** O campo do tipo data que define a data de início do item do cronograma.
- **Data Fim:** O campo do tipo data que define a data final do item do cronograma.
- **Duração:** O campo do tipo inteiro que define a duração em dias do item do cronograma.
- **Predecessor:** O campo do tipo item relacionado que irá conter a referência para o predecessor do item do cronograma.
- **Cálculo Data Final:** Define como será feito o cálculo da data final após somados os dias de duração a data de início. Se nada for selecionado, os dias apenas serão acrescentados a data inicial. Se for selecionado "Dias úteis", os finais de semana serão pulados no calculo. E se for selecionado "Dias corridos (último dia útil)", os dias serão acrescentados a data inicial sem pular finais de semanas, mas se a data final cair em um sábado ou domingo, ela será deslocada para a próxima segunda-feira.
- **Limitar data final:** Com essa opção marcada nenhuma data final de itens relacionados ao cronograma não pode ser maior que a data final definida no próprio cronograma.


###Arquivos

Configurações referentes aos arquivos que podem ser anexados ao item. Não se aplica a tipos da natureza formulário e cronograma.

- **Permite Anexar Arquivos:** Para habilitar a inclusão de arquivos no item. Quando marcada abre a opção **Bloquear Extensões** que permite listar, separadas por vírgula, as extensões de arquivos que não podem ser anexados ao item.
- **Permitir múltiplos arquivos:** Com essa opção habilitada será permitida a inclusão de mais de um arquivo por item. Exemplo de aplicações são documentos com múltiplas folhas ou documentos com uma versão editável e outra em formato neutro. Quando esta opção é marcada, novas opções específicas para itens multi arquivo são abertas:
    - **Exigir arquivo de publicação além do editável:** Exige que o item tenha um arquivo editável em outro em formato neutro, por exemplo, um dwg e um pdf.
    - **Restringir formatos editáveis para o perfil:** Permite que apenas usuários do [perfil] selecionado no combo acessem os arquivos em formato editável, outros usuários visualização apenas os formatos neutros.
    - **Modificar nome dos arquivos para a regra de download do item:** Com esta opção marcada, ao fazer o download de um arquivo ele será baixado com o nome do item ao invés do nome original do arquivo.
    - **Ao gerar revisão, não manter os arquivos da revisão antiga na nova revisão:** Se esta opção for marcada, quando uma nova revisão do documento for gerada, os arquivos não serão associados à nova revisão, ficando apenas como histórico na revisão anterior.
- **Arquivo opcional:** Com esta opção desmarcada, o usuário é obrigado a inserir um arquivo no momento da criação do item.
- **Incluir a Descrição ao nome do arquivo ao exportar:** Com esta opção marcada, quando o usuário fazer o download do arquivo, ele será baixado com o nome do item mais os valores dos campos configurados na [coluna descrição].
- **Utilizar nome do arquivo como nome do item:** O nome do item no GreenDocs será o nome do arquivo que estiver sendo carregado. Quando marcada, habilita a opção "Preencher metadados conforme código do arquivo" que lê o nome do arquivo e extrai automaticamente as informações para preenchimento nos campos que fazem parte da [regra de codificação].
- **Copiar nome do arquivo para:** Copia o nome do arquivo sendo carregado para o campo selecionado na lista.
- **Utilizar campo alternativo para nome:** Considera para efeitos de comparação (verificar se um documento existe no momento de atualizar, por exemplo) e como nome para download do arquivo o valor que estiver preenchido no campo selecionado, ao invés do nome do item.
- **Copiar campos do arquivo:** Com esta opção marcada, no momento do upload de um XML, o Greendocs procura dentro do arquivo os caminhos especificados no tipo de item e copia o valor encontrado para o metadado configurado. Está funcionalidade está disponível também na atividade de criar documentos através da leitura de e-mails.


###Propriedades

É onde os campos do ambiente são associados ao tipo de item. Podem ser inseridas um campo de cada vez, link "Associar Propriedade" ou todos os campos de um conjunto, link "Associar Conjunto". Os campos podem ser marcados como "Obrigatório" e/ou "Somente Leitura", mas as configurações feitas no mapeamento de campos as [atividades de fluxo] sobrescrevem essa configuração. Para mudar a ordem dos campos na tela, tanto na configuração quanto para o usuário final, utilize os ícones de setas para cima e para baixo. Para remover um campo do tipo de item, clique no link "Desassociar" ao lado do nome do mesmo.

###Regra de Codificação

Configuração da regra que irá gerar um código automático para o item no momento de sua criação no GreenDocs.

- **Usar Codificação Manual:** Desabilita as configurações de regra de código e deixa o campo "Nome" aberto para que o usuário preencha o mesmo manualmente.
- **Posições do Código:** A regra de codificação propriamente dita. Selecione na lista um campo do Greendocs e o seu valor será copiado automaticamente para aquela posição do código, um caracetere separador e clique em adicionar. Para a lista de posições possíveis, em caso de um campo do tipo [lista com sigla] ou nas opções Nível 1, Nível 2 e Nível 3, que refletem níves na estrutura de [locais], a sigla do mesmo será utilizada no código ao invés do seu valor completo. Além dos campos, as seguintes opções estão disponíveis:
    - Sequência: A posição no código onde estará o sequencial.
    - Revisão: O número da revisão atual do item. Quando uma nova revisão é gerada, o nome do item é atualizado.
    - Ano(xx): Os últimos dois caracteres do ano atual.
    - Ano(xxxx): O ano atual.
- **Posições da Chave:** A configuração funciona da mesma maneira que as posições de código e a combinação dessas posições será utilizada como chave para geração do sequencial.
- **Utilizar as mesmas posições para a chave:** Utiliza as mesmas posições do código para a chave (posições sequencia e revisão são ignoradas). 
- **Copiar sequencial para campo customizado:** Copia o número do sequencial gerado para um campo. Esta opção é útil quando o valor do sequencial necessite ser utilizado em outras situações, como [triggers] e na utilização na geração de código de itens referenciados.
- **Configuração do Sequencial:** Configurações específicas do sequencial gerado para o código.
    - Nº de casas: Define o número de casas para o sequencial gerado.
    - Utilizar Sequência Manual: Não gera um sequencial automatico e abre o campo sequencial para que o usuário preencha o número no momento da criação do item. Quando marcado abre a opção "Display Name Sequência", onde pode ser definido o nome com o qual o campo do sequencial será exibido para os usuários.
    - Permitir alteração manual da sequência gerada automaticamente: Permite que o usuário com permissão altere manualmente a sequencia que foi gerada automaticamente pelo GreenDocs.
    - Guardar sequencia para documentos apagados: Utilizada para reaproveitamento de sequenciais. O sequencial de um documento apagado será reservado para utilização pelo próximo documento que contenha a mesma chave. Permite definir uma atividade de fluxo após a qual o sequencial do documento não poderá mais ser reaproveitado.
    
###Coluna Descrição

A coluna descrição pode ser utilizada com coluna nas exibições do GreenDocs. Ela consiste na combinação de diversos campos que são exibidos para o usuário como uma única sequencia de caracteres. Ao ativar a coluna na configuração, os campos que irão fazer parte da coluna descrição podem ser adicionados e reordenados na lista. A coluna descrição na duplica os valores dos campos nos itens, seu valor é calculado em tempo real, ou seja, se um dos valores de um dos campos for alterado, a coluna descrição refletirá automaticamente este valor.

###Exportação

Configuração da exportação de formulários para impressão ou para os formatos Excel, PDF ou Word. Por padrão os formulários permitem a exportação para impressão (HTML), XLS e PDF. Essa exportação simplesmente exporta os campos do formulário em um formato mais simples, sem os outros elementos da interface do GreenDocs. Modelos de exportação customizados também podem ser utilizados, e se necessário os modelos padrão podem ser suprimidos da interface com a opção "Ocultar opções padrão de exportação".

Os modelos podem ser no formato DOCX ou XLSX e precisam ser previamente preparados para utilização como modelo no GreenDocs.

####Excel

Para inserir um modelo de exportação para Excel, clique em "Adicionar Modelo" e faça o upload do arquivo com extensão xlsx. Após inserido o modelo, clique em "Configurar". Na seção "Cabeçalho", faça o mapeamento das células do Excel com campos do GreenDocs selecionando o nome da planilha onde serão preenchidos os dados, a célula (ex: A1), e o campo do GreenDocs. Caso seja um campo do tipo lista com sigla, marque a opção "Somente Sigla" para que apenas a sigla do valor preenchido no campo seja exportada para o excel.

Se for necessário exportar uma lista de itens referenciados para a planilha (uma LD ou GRD, por exemplo), na criação do modelo devem ser definidos dois nomes na planilha.

O primeiro indicando o cabeçalho da lista que irá trazer os itens referenciados (apenas a linha com o cabeçalho deve ser selecionada).

![header](http://publicado.greendocs.net.s3.amazonaws.com/docs/img_excel_01.png "Exemplo de configuração do cabeçalho")

Outro uma área de impressão com tamanho dinâmico. Isto é feito utilizando as fórmulas abaixo.

Fórmula para Excel em português:

	=DESLOCAMENTO(Sheet1!$A$1; 0; 10; CONT.VALORES(Sheet1!$A:$A); 1)

Fórmula para Excel em inglês:

	=OFFSET(Sheet1!$A$1, 0, 0, COUNTA($A:$A), 1)
    
Após inserido o modelo, na configuração do modelo, selecione o primeiro nome definido anteriormente na opção "Nome tabela itens referenciados". A seguir selecione na lista de colunas o nome da coluna no excel e o campo no GreenDocs referente a esta coluna. Para cada item referenciado ao item sendo exportado, será inserida uma linha no excel com os valores dos campos configurados.

####Word

Um modelo em formato Word precisa ser previamente preparado com a marcação de posições no texto que deverão ser substituídos por valores de campos do item sendo exportado.

Abra o Word e habilite a faixa de opções de Desenvolvedor, acessando Opções -> Personalizar Faixa de Opções e no lado direito, em Guias Principais, marque a opção "Desenvolvedor". Feche a janela de opções e abra o arquivo Word que servirá como modelo.

**Mapeamento dos campos**

Com o arquivo modelo aberto no Word, clique em "Desenvolvedor" e então em "Modo de Design". Posicione o cursor no local onde o campo deverá ser inserido e clique na opção de "Controle de conteúdo de texto sem formatação" ou "Controle de conteúdo rich text".

![modelo_word](./img_word_01.png "Adicionando controle de conteúdo no modelo")

Clique com o botão da direita sobre o controle inserido e então em propriedades. Nas propriedades do controle,  preencha como título o nome interno de um campo conforme configurado no GreenDocs. Os campos podem ser inseridos no corpo do word ou no cabeçalho e rodapé. Sai do modo de design, salve o arquivo e faça o upload para a configuração de modelos do tipo de item.

**Opções de mapeamento avançadas**

Além do nome interno de campos customizados, outros valores podem ser exportados para o word:
- Nome: Para exportar o nome do item, preencha como título nas propriedades do controle de conteúdo o valor "Nome".
- Atividade: Para exportar o nome da atividade atual do item no fluxo, preencha com o valor "Atividade".
- Processo: Para exportar o nome do processo (fluxo) do item, preencha com o valor "Processo".
- Dados de um usuário: Para um campo do tipo lista de usuários, é possível exportar dados das [informações adicionais] do usuário, para isso preencha o valor do título com os nomes internos do campo lista de usuários e do campo do [cadastro de usuários] separados por um ".". Ex: "nome_pessoa.cpf".
- Grupo de um usuário: Para um campo do tipo lista de usuários, é possível exportar o nome do grupo do usuário selecionado como valor no campo, para isso preencha o valor do título com o nome interno do campo lista de usuários e o sufixo ".Grupo". Ex: "nome_pessoa.Grupo".
Dados de um grupo: Para um campo do tipo lista de grupos, é possível exportar dados das [informações adicionais] do grupo, para isso preencha o valor do título com os nomes internos do campo lista de grupos e do campo do [cadastro de grupos] separados por um ".". Ex: "nome_grupo.razao_social".
- Datas: Algumas opções de data são possíveis, preencha o título da propriedade do controle de conteúdo comn
    - Data - Para escrever a data atual
    - DataExtenso - Para escrever a data atual por extenso
    - Mes - Para escrever o nome do mês atual
    - Ano - Para escrever o ano atual
    - AnoSeguinte - Para escrever o ano atual + 1
- Itens referenciados: Uma tabela pode ser adicionada no word para receber os valores de itens referenciados, para isso na primeira linha da tabela, faça o mapeamento dos campos normalmente, porém com o prefixo "itemref.". Por exemplo, para exportar o nome do item referenciado, preencha o título nas propriedades do controle de conteúdo como "itemref.Nome".
- Tabelas: Para exportar um campo do tipo tabela, adicione uma tabela ao word e na primeira linha desta faça o mapeamento dos campos preenchendo como título do controle de conteúdo o nome interno do campo da tabela e o nome interno do campo da coluna separados por uma "/". Ex: "tabela_a/coluna_1".

Na lista de modelos ainda existem as opções de "Atualizar", para fazer o upload de uma versão nova do modelo, "Abrir" para baixar uma cópia do modelo, "Remover" para excluir o modelo.

###Tipos Relacionados

Configuração dos tipos de item que podem ser relacionados como "filhos" do tipo de item atual. Para adicionar um novo tipo clique em "Relacionar tipo de Item", selecione o tipo de item, e clique em adicionar. Opcionalmente pode ser selecionado um [tipo de referência] que servirá para agrupar os itens na lista de referências.

A opção "Copiar valores dos campos" fará com que os campos que sejam compartilhados entre os dois tipos de item sejam copiados do item pai para o item filho.

Após adicionar um tipo de item, clicando em "Regras" é possível configurar condições para quando e como os itens podem ser relacionados:
- **Permitir criar a partir daqui:** Define que, estando em um item deste tipo, é possível criar um novo item do tipo relacionado. Ao marcar esta opção, uma nova configuração é habilitada, "Criação simplificada", se marcada isto fará com que a tela de criação do novo item abra em um pop-up, não havendo a necessidade de sair da tela do item atual para criar um novo item referenciado.
- **Permitir relacionar a partir daqui:** Define que, estando em um item deste tipo, é possível referenciar itens existentes do tipo relacionado.
- **Metadado:** Os campos adicionados aqui precisarão ter valor igual preenchido tanto no item pai quanto no item filho para que os mesmos possam ser relacionados.
- **Atividades em que o item pode ser relacionado:** Atividades em que o item relacionado (filho) precisa estar para que ele possa ser referenciado. Adicionando mais de uma atividade, o item poderá estar em qualquer uma delas.
- **Atividades do item origem em que o item pode ser criado/referenciado:** Atividades em que o item (pai) precisa estar para que ele possa ter itens referenciados ou criados. Adicionando mais de uma atividade, o item poderá estar em qualquer uma delas.

Em "Permissões" são definidos quais usuários terão acesso ao referenciamento destes tipos de item. Esta permissão é configurada através de uma [regra de posições].

###Notificações

Configuração das notificações por e-mail relacionadas ao tipo de item.

- **Modelo para notificações de comentário:** O [modelo de e-mail] que será utilizado para notificar os usuários quando um comentário for adicionado ao item.
- **Notificar responsáveis quando um comentário for adicionado ao item:** Irá enviar um e-mail para o responsável pelo item, utilizando o modelo configurado anteriormente, quando um novo comentário for adicionado ao item.
- **Campos de usuário para notificação:** Irá enviar uma e-mail de notificação para os usuários que estiverem selecionados como valor dos campos do tipo lista de usuários selecionados aqui.

###Controle de horas

Opções específicas para o tipo de item quando o controle de horas estiver habilitado para este tipo. Outras opções estão disponíveis na configuração de [controle de horas](ambientes->avancado->controledehoras) do ambiente.

- **Campo lista de usuários do controle de horas:** Se um campo do tipo lista de usuários for selecionado aqui, no momento do registro das horas trabalhadas, o registro poderá ser feito em nome do usuário que estiver selecionado neste campo. Se o campo for uma coluna de um campo do tipo tabela, uma quantidade de horas diferente poderá ser preenchida para cada um dos usuários inseridos como linhas da tabela.
- **Permitir controle apenas do dia atual:** Se marcada exibe apenas a opção de selecionar as horas de início e fim no momento do registro, caso contrário permite a seleção das datas.
- **Controle de horas automático:** Se marcado, irá acionar automaticamente o contador de horas quando o usuário abrir um arquivo anexado ao item.
- **Regras:** Configuração de regras que definem quando o contador de horas pode ser ativado para o item. Atualmente, a regra possível de ser adicionada é verificar se um item referenciado acima do item atual na hierarquia de referências está ou não está em uma atividade ou evento de fluxo específico.

###Templates

Para tipos de item da natureza Documento é possível definir templates, modelos de arquivo que serão anexados automaticamente ao documento criado. Quando o usuário criar um item novo, a lista de templates será apresentada para seleção. Após criar o documento, ele pode abrir o arquivo criado e editar a partir do modelo. Qualquer tipo de arquivo pode ser usado como modelo.