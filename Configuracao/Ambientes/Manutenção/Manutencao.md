#Ambientes - Manutenção
##Sequenciais
O gerenciamento de sequenciais contém uma lista de todas as chaves de código onde itens já foram criados pelo menos uma vez. Podem ser alteradas as sequências de criação do próximo item. 
O número de sequencia apresentado na tela representa o número do próximo item a ser criado.
##Exclusão definitiva de itens
Apresenta a lista de itens excluídos pelos usuários, que podem ser excluídos definitivamente ou serem restaurados para os usuários. 
Pode-se pensar nesta aba de configuração como a "lixeira" do sistema.
##Exportação de dados
A exportação de dados é um recurso oferecido aos clientes para assegurar a segurança patrimonial da informação em um ambiente de nuvem.

Esse recurso exporta os arquivos e as propriedades de cada um dos tipos de item em um formato neutro, e compacta em um ZIP.

O agendamento é executado da seguinte maneira:

Pressiona-se o botão "Agendar Exportação" e seleciona-se se as revisões antigas devem ser incluídas ou não.

A lista apresenta o item agendado sem data juntamente com um ícone de relógio. Isso significa que na próxima noite a geração será executada.
No dia seguinte, ao acessar a mesma tela você verá a data preenchida e um ícone de download. 
Após clicar no ícone, o download irá começar. Esse processo pode demorar, dependendo da velocidade da sua conexão com a internet e do tamanho do seu ambiente.
##Geração de Thumbnail (Visualização)
Gera os thumbnails das imagens e arquivos dwg inseridos no Greendocs e habilita a visualização.
##Índices
O índice é um recurso utilizado para tornar a pesquisa mais rápida. Nesta tela você vê as estatísticas da indexação apenas para referência. Utilize o botão de atualizar índice se você julgar que existe algum item no qual a pesquisa não esteja funcionado corretamente.
##Responsabilidades em documentos
Atualiza as responsabilidades de documentos em uma determinada etapa/ atividade.