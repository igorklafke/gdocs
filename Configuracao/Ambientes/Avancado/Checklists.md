#Configuração do Ambiente - Avançado

##Checklists

Um checklist é uma lista de documentos "templates", considerados obrigatórios para uma determinada entidade ou atividade. Ao criar um checklist, o usuário tem o controle de quais documentos previstos na lista já foram inseridos no sistema e quais ainda estão pendentes.

Para criar um novo Checklist, clique em "Cadastrar Novo", dê um nome e clique em salvar.

Para definir os documentos que servirão de modelo para o checklist, é necessário primeiro criá-los pela interface de usuário. Após, acesse o checklist desejado na configuração e clique em "Editar itens associados" e então em "Associar Item". Uma tela para pesquisa será exibida, busque pelos documentos criados anteriormente, selecione os que deseja transformar em modelo e clique em "Adicionar Selecionados".

Os documentos de modelo que forem associados a um checklist, não aparecerão mais para os usuários. Quando o usuário criar este checklist, novos documentos vazios serão criados com os valores de campos copiados dos itens modelo. Ao acessar um checklist, o usuário poderá verificar quais destes itens já foi atualizado com conteúdo e qual ainda está pendente.

O link "Usuários que podem criar item via checklist", leva para a interface de configuração de permissões para que se possa definir os usuários que podem criar itens por um checklist.
