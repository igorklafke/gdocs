#Configuração do Ambiente - Avançado

##Planilha

A configuração de importação de planilhas é utilizada para importação a partir de planilhas de Excel. Nesta tela são configurados os parâmetros, como mapeamento de colunas para campos, e a importação é realizada através de um executável externo.

Para configurar uma nova importação, clique no link "Nova Planilha" no topo da lista de planilhas. Uma nova tela será exibida com as seguintes opções:

- **Nome:** O nome da planilha (sheet) conforme configurado no excel.
- **Tipo Item:** O tipo de item no GreenDocs em que os documentos importados serão criados.
- **Processo:** O processo (fluxo de trabalho) onde os documentos serão inseridos.
- **Tipo Item da GRD:** Se a planilha sendo configurada for uma GRD/GR/GRE, selecione o tipo de item do GreenDocs que irá utilizar esta importação de planilha. Ao criar um item deste tipo no GreenDocs, e selecionar uma planilha, os documentos e seus dados serão importados do excel de acordo com a configuração desta planilha.
- **Linha Inicial:** A linha da planilha que contém o primeiro item a ser importado.
- **Extrair Valores Metadados do Nome:** Com essa opção marcada, se o nome do documento na planilha estiver no padrão configurado no GreenDocs, campos que fazem parte da regra serão preenchidos automaticamente, extraindo esta informação do código do documento.
- **Planilha de Migração:** Marque esta opção quando a configuração se tratar de uma planilha para migração de legado, com possiblidade de múltiplas revisões por documento na mesma planilha.
- **Pasta:** A pasta (local) onde os documentos importados serão criados no GreenDocs.

Após salvar, a nova planilha é exibida na lista. Clique em "Regras" para configurar o mapeamento das colunas da planilha com os campos para onde os dados serão importados no GreenDocs. Por padrão, a configuração já traz uma coluna para o nome do documento e outra para o caminho onde se encontra o arquivo que será importado, você pode editar estas entradas para alterar o número da coluna, se necessário.

Para adicionar uma nova coluna, digite o número da mesma no campo "Coluna", isto deve ser um número e não uma letra como no Excel. A coluna A, por exemplo, deve ser preenchida nesta configuração como 1. Após, se a coluna for um campo configurado no GreenDocs selecione o mesmo em "Propriedade", caso contrário utilize o campo "Opção" para os seguintes casos:

- **Protocolo:** A coluna indica o nome do protocolo (GRD/GR/GRE) do qual o documento faz parte, se já existir um protocolo com o nome preenchido nesta coluna o documento será inserido nele, caso contrário um novo protocolo será criado com este nome e o documento associado a ele.
- **Revisão Item:** Número da revisão do documento.
- **Responsável:** Usuário que será o responsável pelo documento no GreenDocs.
- **Item Relacionado:** Nome de um item que deve ser referenciado ao documento. Este item pode já existir no GreenDocs ou ser um item na planilha, desde que esteja, na ordem, antes do documento sendo processado.
- **Atividade:** Atividade do fluxo de trabalho onde o documento deve ser inserido.