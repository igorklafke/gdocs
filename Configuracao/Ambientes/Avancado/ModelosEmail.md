#Configuração do Ambiente - Avançado

##Modelos de e-mail

Os modelos de e-mail são templates que podem ser utiliados através do ambiente nas notificações de e-mail. Os modelos configurados aqui podem ser utilizados nas configurações de notificações dos fluxos, comentários realizados e notificações de prazo em lote. O ambiente possui alguns modelos padrão para notificações que podem ser alterados, além disso modelos customizados também podem ser criados para substituir ou complementar a estes modelos.

###Editar um modelo

Clique sobre o modelo que deseja editar. O template do e-mail será carregado e você poderá editar o conteúdo conforme desejar.

Como um e-mail de notificação geralmente refere-se a um documento ou formulário do sistema, você pode utilizar marcadores representando campos do documento que posteriormente serão dinamicamente subsituídos pelos valores propriamente ditos. Para adicionar um campo, posicione o cursor no local do texto que deseja adicionar o campo e clique no botão "Inserir Campo" à direita do conteúdo do e-mail e selecione uma das opções:

- **Atributos do item:** Para campos internos do item, como nome, tipo de item, processo, situação ou o último comentário realizado.
- **Atributos de notificação:** Campos que se referem ao e-mail que está sendo enviado. Como o usuário que gerou a ação que acarretou neste envio de e-mail, o nome do destinatário da notificação, a etapa de origem ou de destino e o nome da ação realizada, estes três últimos para o caso de uma notificação envolvendo fluxo de trabalho.
- **Campos:** As demais opções são os conjuntos de campos configurados no ambiente. Ao selecionar um conjunto a lista de campos daquele conjunto é mostrada.

Após selecionar um campo, clique em adicionar e o marcador será inserido no texto na posição onde estava o cursor. Você pode alterar manualmente este texto para reposicionar o marcador conforme a necessidade.


###Criar um modelo customizado

Para criar um modelo customizado, clique no link "Criar Modelo Customizado" no topo de lista de modelos de e-mail e um template padrão será carregado. Você pode alterar este modelo da mesma forma que edita um modelo existente. Dê um nome para o modelo e clique em "Salvar"

Diferentemente dos modelos padrão, um modelo customizado pode ter o seu assunto alterado da mesma forma que o corpo do e-mail. Por isso, do lado direito da tela existem dois botões "Inserir Campo", um para o assunto e outro para o corpo do e-mail.