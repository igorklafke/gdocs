#Configuração do Ambiente - Avançado

##Triggers

No GreenDocs, *triggers* são ações que podem ser realizadas automaticamente de acordo com a ocorrência de eventos pré-configurados.

A combinação de eventos e ações existentes dá ao configurador o poder de realizar tarefas complexas através do sistema sem a necessidade de criação de scripts. 

As ações de uma trigger são executadas unicamente sobre o item que gerou o evento da trigger, a não ser quando especificado na ação que ela será executada sobre um item relacionado. É importante ressaltar também que triggers podem ser encadeadas, por exemplo, se a ação de uma trigger altera o valor de um campo, triggers que possuírem como evento a alteração deste campo serão disparadas em sequencia.

Para criar uma nova trigger, utilize o link "Nova Trigger" localizado no topo da lista de eventos. Para editar uma trigger existente, clique no nome na lista de eventos para exibir todas as triggers daquele agrupamento e então sobre o nome da trigger na lista do lado direito.

###Cadastro de uma trigger

A descrição de uma trigger servirá como um agrupador na interface de configuração, triggers com a mesma descrição aparecem como um único item na lista de eventos. É prática comum utilizar esta descrição para agrupar as diferentes triggers que envolvam uma mesma parte do ambiente, como um tipo de item ou processo, por exemplo.

A configuração de uma trigger é dividida em "Quando", "Se" e "Então". "Quando" é o evento que faz a trigger ser executada. "Se" são as condições adicionais que devem ser atendidas para que as ações sejam executadas. "Então" são as ações que serão realizadas.

####Eventos

- **Ação BPM executada:** Disparada quando uma ação de fluxo é executada. É preciso selecionar o processo a atividade e a ação.
- **Agendamento:** Disparada diariamente no horário especificado. Utilizada para notificações em lote, especificamente as ações "Notificar responsáveis em lote sobre prazo dos documentos" e "Notificar usuários de distribuição em lote sobre prazo dos documentos".
- **Ao referenciar item:** Disparada quando uma referência é criada entre itens do ambiente.
- **Arquivo atualizado:** Executada quando um arquivo for adicionao ao item ou quando um arquivo existente for substituído.
- **Cancelamento de Item Desfeito:** Disparada quando o usuário desfaz o cancelamento de um item.
- **Item Atualizado:** Disparada quando algum campo do item é alterado.
- **Item Cancelado:** Disparada quando o usuário faz o cancelamento do item.
- **Item Excluído:** Disparada quando item é excluído pelo usuário.
- **Metadado Atualizado:** Executada quando um campo do item é alterado. É preciso selecionar na lista o campo cuja alteração dispara o evento.
- **Novo Item Criado:** Disparada no momento em que o item é recém criado.
- **Novo sub-processo criado:** Disparada quando um sub-processo é criado dentro de um outro processo.

####Condições

Existem duas opções de condição, uma trigger poder ser executada "Sempre" ou quando "Todos os critérios forem atendidos". Os critérios disponíveis são:

- **Metadado:** Condições relacionadas à valores de campos do item. Seleciona-se o metadado (campo) e se ele deve ser igual a, diferente, maior que, menor que, maior ou igual a ou menor ou igual a um valor. Para campos do tipo data, a comparação é feita com outros campos do mesmo tipo, e também é possível adicionar dias extras para a comparação, este número de dias pode ser também negativo.
- **Data atual:** Compara a data atual com campos do tipo data, assim como no metadado os operadores possíves são igual a, diferente, maior que, menor que, maior ou igual a ou menor ou igual a.
- **Tipo de Item:** Verifica o tipo de item sobre o qual o evento foi gerado, executando a ação somente se o item for do tipo selecionado na condição.
- **Tipo de Item Referenciado:** Condição utilizada para verificar o tipo de item do documento que estiver sendo referenciado no evento "Ao referenciar item".


####Ações

- **Atualiza campo com valor calculado:** Atualiza o valor de um campo do tipo número ou moeda utilizando uma fórmula. Esta fórmula pode envolver operações matemáticas com outros campos do item, sendo o nome interno destes outros campos as variáveis utilizadas na fórmula.
- **Atualiza campo com valor de outro campo:** Copia o valor de um metadado (campo) para outro. O campo destino deve ser do mesmo tipo que o campo de origem.
- **Atualiza campo com valor fixo:** Preenche o valor do campo com um valor fixo. Aplicável apenas para campos do tipo string.
- **Atualiza campo data (A partir de outro campo - campo inteiro):** Preenche o valor de um campo data com o valor de outro campo data menos um número de dias 
preenchido em um campo do tipo inteiro.
- **Atualiza campo data (A partir de outro campo + campo inteiro):** Preenche o valor de um campo data (metadado destino) com o valor de outro campo data (metadado origem) mais um número de dias preenchido em um campo do tipo inteiro (dias).
- **Atualiza campo data (A partir de outro metadado):** Preenche o valor de um campo data (metadado destino) com o valor de outro campo data (metadado origem) mais um número fixo de dias, que pode ser negativo ou zero.
- **Atualiza campo data (Com a data atual + Outro metadado):** Preenche o valor de um campo data (metadado destino) com a data atual mais um número de dias preenchido em um campo do tipo inteiro (dias).
- **Atualiza campo data (Com a data atual):** Preenche o valor de um campo data (metadado destino) com a data atual mais um número fixo de dias, que pode ser negativo ou zero.
- **Atualiza campo em item referenciado:** Copia o valor de um campo do item (metadado origem) para um campo do mesmo tipo no item referenciado (metadado destino). Deve ser utilizado em conjunto com o evento "Ao referenciar item" ou com a condição "Tipo de Item Referenciado".
- **Atualiza situação:** Altera a informação de situação na revisão atual do item.
- **Atualizar lista/lista sigla:** Altera o valor de um campo do tipo lista ou lista sigla para o valor selecionado em "Preencher".
- **Atualizar Situação em item referenciado:** Altera a informação de situação na revisao atual no item referenciado. Deve ser utilizado em conjunto com o evento "Ao referenciar item" ou com a condição "Tipo de Item Referenciado".
- **Chamar WebService:** Executa uma chamada a um WebService externo. A lista vem dos webservices externos configurados no GreenDocs. A opção "Executar se origem WS" define se esse web service deve ser executado mesmo se o evento que iniciou a trigger tenha sido disparado através do WebService (API) do GreenDocs.
- **Criar/atualizar item em outro projeto:** Copia os dados do item que gerou e evento e cria um item em outro ambiente, no processo, etapa e tipo de item selecionado. Se já existir um item com o mesmo nome no outro ambiente, ele é atualizado.
- **Executar ação de fluxo:** Executa a ação de fluxo (decisão) selecionada sobre o item que gerou o evento.
- **Incrementa campo:** Soma 1 ao valor de um campo do tipo inteiro (metadado destino).
- **Limpa campo:** Remove o valor preenchido do campo selecionado (metadado destino).
- **Preencher campo com Usuário logado:** Define o valor de um campo do tipo lista de usuários (metadado destino) para o nome do usuário logado no momento da geração do evento que iniciou a trigger.
- **Preencher campo com Unidade do Usuário:** Define o valor de um campo do tipo lista de unidades (metadado destino) para a primeira unidade na qual o usuário tenha uma posição atribuída.
- **Notificar responsáveis em lote sobre prazo dos documentos:** Aplicável apenas quando o evento for de "Agendamento". Envia um e-mail para os responsáveis de documentos que estejam com prazo vencido ou próximo de vencer. Ver modelos de e-mail para configurar o template do e-mail que será enviado.
- **Notificar usuários de distribuição em lote sobre prazo dos documentos:** Aplicável apenas quando o evento for de "Agendamento". Envia um e-mail para os usuários de distribuição atribuidos a documentos que estejam com prazo vencido ou próximo de vencer. Ver modelos de e-mail para configurar o template do e-mail que será enviado.