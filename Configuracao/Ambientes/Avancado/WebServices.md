#Configuração do Ambiente - Avançado

##WebServices

A configuração de Web Services permite adicionar APIs http para serem chamadas pelo GreenDocs através de [Triggers], possuindo a opção de enviar dados do documento no GreenDocs como parâmetros para esta API.

Para adicionar um novo WebServices clique no link "Novo WebServices" no topo da lista dos WebServices existentes, preencha a URL da api a ser executada (parâmetros fixos devem ser adicionados diretamente na URL) e clique em Salvar. Para definir os parâmetros dinâmicos, clique sobre a URL do Web Service e em seguida no link "Novo Parâmetro" que irá aparecer no lado direito da tela. Preencha o nome do parâmetro e escolha o tipo, Id Documento ou Metadados. O "Id Documento" é um identificador único do item no GreenDocs, e "Metadados" são os campos do documento no GreenDocs e que serão enviados em formato JSON como parte do corpo da requisição HTTP para o WebService.