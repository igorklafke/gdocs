#Configuração do Ambiente - Avançado

##Controle de Horas

O controle de horas do GreenDocs permite que os usuários registrem manual ou automaticamente a quantidade de horas trabalhadas sobre um documento.

Este controle de horas deve ser habilitado por tipo de item, na configuração "Geral" do mesmo, marcando-se a opção "Habilitar Controle de Horas Trabalhadas". Também no tipo de item encontra-se a configuração para controle de horas automático, onde um contador é iniciado automaticamente quando um arquivo do documento é aberto.

Com o controle de horas habilitado, o usuário que for responsável pelo documento poderá utilizar as opções de iniciar e parar o contador de horas ou preencher a quantidade de horas manualmente.

Para controle manual, o registro pode ser configurado para ser realizado em **Horas**, onde o usuário irá preencher o número de horas trabalhadas no documento, ou por **Período**, onde o usuário irá preencher a data e hora inicial e final.

A configuração de "Quantidade máxima de horas por dia" permite estabelecer um limite de horas que um usuário pode trabalhar dentro de um período de 24 horas. Se este valor estiver preenchido com zero, não haverá limite.

O "Atraso máximo para preenchimento" define o número de dias de atraso em que o usuário pode cadastrar as horas trabalhadas no caso do preenchimento por período. Se este valor estiver preenchido com zero, o usuário é obrigado a preencher as horas no mesmo dia, não sendo possível escolher um período com uma data anterior a atual.

Com a opção "Não permitir inserção manual de horas", o usuário não poderá inserir manualmente a quantidade de horas trabalhadas, seja pelo número total de horas ou por período, as horas poderão ser registradas apenas pelas opções de inciar e parar o contador ou automaticamente ao abrir o arquivo, se esta opções estiver habilitada no tipo de item.

Ao marcar "Ativar Preenchimento de Evento", uma opção para inserir eventos é habilitada. Estes eventos aparecerão como opção para o usuário selecionar quando este for registrar as horas trabalhadas, asssim é possível classificar o tipo de trabalho que foi realizado.

For fim, as "Regras de usuários sem restrições" definem quais usuários podem cadastrar horas sem os limites definidos em "Quantidade máxima de horas por dia" e "Atraso máximo para preenchimento". As regras são definidas pela posição do usuário, adicionando uma combinação de Unidade, Área, Função e Grupo. [Veja como funcionam as configurações de regra por posições]