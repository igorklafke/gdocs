#Configuração do Ambiente - Avançado

##LDs

A configuração de Listas de Documentos (LD) tem comportamento semelhante à [Planilha], exceto por ser um procedimento realizado pelo usuário do GreenDocs com permissão para tanto. A LD pode ser um item cadastrado no GreenDocs ou apenas um procedimento para importação das planilhas recebidas pelo cliente. Para configurar uma LD clique no link "Nova LD", uma nova tela será exibida com as seguintes opções:

- **Nome:** O nome que identifica a LD para o usuário.
- **Tipo de item dos itens:** O tipo de item no GreenDocs em que os documentos da lista importada serão criados.
- **Tipo de item da lista:** O tipo de item da LD. Este campo não é obrigatório, se preenchido a LD deverá ser importada pelo usuário a partir do item do tipo selecionado aqui, se não, será importada diretamente a partir do menu "Adicionar" e não haverá um item do GreenDocs agrupando os itens da LD.
- **Nº Revisão:** 
- **Nome da Planilha:** O nome da planilha (sheet) conforme configurado no excel.
- **Linha Inicial:** A linha da planilha que contém o primeiro item a ser importado.
- **Pode Cancelar:** Indica se um item pode ser cancelado a partir da importação da LD. Se marcada, abrirá opção para definir a etapa de fluxo para onde o documento cancelado será encaminhado e também a situação da revisão cancelada. Além disso, posteriormente na configuração das regras da planilha deve-se definir a coluna do excel que indica o item como cancelado.
- **Preencher metadados conforme código do arquivo:**
- **Formar o código a partir das colunas da planilha:** Com esta opção marcada, o código do documento será gerado automaticamente no momento da importação a partir das colunas da planilha que fazem parte da regra de codificação, caso contrário o nome do documento ficará conforme a coluna Nome configurada posteriormente nas regras.
- **Pasta:** A pasta (local) onde os documentos importados serão criados no GreenDocs.

Após salvar, a nova LD é exibida na lista. Clique em "Regras" para configurar o mapeamento das colunas da planilha com os campos para onde os dados serão importados no GreenDocs. Por padrão, a configuração já traz uma coluna para o nome do documento e outra para o caminho onde se encontra o arquivo que será importado, você pode editar estas entradas para alterar o número da coluna, se necessário.

Para adicionar uma coluna, clique no link "Nova Coluna", digite o número da mesma no campo "Coluna", isto deve ser um número e não uma letra como no Excel. A coluna A, por exemplo, deve ser preenchida nesta configuração como 1. Após, se a coluna for um campo configurado no GreenDocs selecione o mesmo em "Propriedade", caso contrário utilize o campo "Opção" para os seguintes casos:

- **Nome Item:** Nome do documento.
- **Revisão Item:** Número da revisão do documento.
- **Emissão Previsa:** Data prevista para emissão do documento.

A caixa de seleção "Permite Atualizar" indica se o valor desta coluna pode sobrescrever o que estiver preenchido no GreenDocs se o documento estiver sendo atualizado.

Se o campo "Texto Cancelamento" for preenchido, indicará que esta coluna controla se o documento deve ser cancelado ou não. Se a coluna na planilha para um determinado item estiver com o mesmo texto deste campo, o item será cancelado.