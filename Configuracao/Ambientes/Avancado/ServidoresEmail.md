#Configuração do Ambiente - Avançado

##Servidores de e-mail

Os servidores de e-mail são contas de e-mail que são monitoradas pelo GreenDocs para cadastro automático. E-mails enviados para estas contas são captados automaticamente pelo GreenDocs e criados como novos itens no ambiente.

Para cadastrar um servidor clique no link "Novo Servidor" e preencha os dados de conexão da conta de IMAP ou POP3. Na configuração do fluxo de trabalho, um evento do tipo [Início por e-mail] deve ser criado e configurado para receber os e-mails deste servidor.