#Configuração do Ambiente - Geral

No GreenDocs definimos "Ambiente" por um conjunto de processos interligados onde cada um destes processos poderá ser realizado por partes distintas.

O Ambiente é a maior unidade de configuração disponível e engloba todas as outras configurações, o que significa que todas as opções de customização disponíveis no GreenDocs estão encapsuladas dentro de um ambiente. Pode-se utilizar mais de um ambiente se o requisito funcional exigir que assim seja.


##Informações Gerais

Nesta tela são configuradas as "Informações Gerais do Ambiente"

- **Nome:** O nome pelo qual o ambiente é reconhecido pelos usuários.
- **Data Início:** Quando um ambiente representa um projeto com início e fim, esta data é utilizada nos relatórios que necessitam de uma data inicial.
- **Sigla:** A sigla do ambiente é utilizada para identificá-lo na interface de configuração e pode ser utilizada nas regras de codificação dos itens.
- **Pasta Raiz:** É a pasta onde todos os outros locais serão criados. Costuma-se utilizar aqui a sigla do ambiente, se esta for de conhecimento dos usuários.
- **Grupo Dono:** É o grupo de usuários responsável pelo ambiente.
- **Idioma:** É o idioma principal do ambiente. O idioma definido neste campo será assumido para todos os usuários que não tiverem a sua própria definição de idioma.

A opção de **Exportar Ambiente** salva todas as configurações do ambiente em um arquivo, quando um novo ambiente estiver sendo criado, este arquivo pode ser utilizado para importar as configurações, essencialmente permitindo a cópia de configuração de um ambiente para outro.


##Revisionamento

Define as regras para revisão dos itens no ambiente. Ao selecionar o tipo de revisionamento pode-se visualizar uma pequena descrição de como ele funciona, cada uma delas pode ter diferentes opções que serão apresentadas de acordo com o valor selecionado. Além destas definições, é necessário configurar no processo/fluxo onde e como serão criadas as revisões dos itens.

Quatro tipos de revisionamento são possíveis:

- **Numérica:** Revisão numérica simples, incrementando 1. Utilize o campo "Iniciar em" para definir o valor da primeira revisão.
- **Alfabética:** Revisão do documento inicia em A e cada incremento sobe uma letra. É possível configurar para que algumas letras sejam ignoradas, é comum em alguns projetos pularem as letras I e O, por exemplo. Para isso, adicione as letras que devem ser ignoradas no campo "Ignorar Caracteres" separadas por ponto e vírgula (;).
- **Composta:** É composta pela revisão cheia e revisão parcial. Acontece quando várias revisões parciais alfabéticas formam uma revisão cheia numérica. Ex: 0A, 0B, 0C, 0, 1A, 1B, 1, ... No processo deverá ser configurado quando que a revisão cheia ou parcial será incrementada. Assim como na revisão alfabética, é possível ignorar caracteres que serão utilizados na segunda parte (letras) do número da revisão.
- **Alfanumérica:** Revisão do documento incia em 0 e incremeta numéricamente até que passe a ser incrementada por letras. Ex: 0, 1, 2, 3, A, B, C, ... No processo deverá ser configurado quando que a revisão é alterada de numérica para alfabética.

Opções adicionais (válidas para todos os tipos de revisionamento):

- **Apresentar confirmação de revisão do upload:** Para os casos onde a revisão é incrementada no upload de um novo arquivo para o item (configuração a ser realizada no processo), marque esta opção para que o usuário seja notificado deste incremento de revisão.
- **Permitir alteração manual da revisão:** Permite que usuários (que possuam permissão) alterem a revisão de um item manualmente. Ao marcar esta opção, uma nova configuração estará disponível "Obrigatório Motivo da Troca de Revisão".
- **Obrigatório Motivo da Troca de Revisão:** Disponível apenas quando a opção de alteração manual da revisão estiver marcada, faz com que o usuário seja obrigado a dar um motivo para esta alteração. Quando marcada, um campo para adicionar uma lista de motivos possíveis é exibido.


###Sub-revisionamento

O sub-revisionamento deve ser configurado através de eventos no fluxo. Ver Processos/Fluxo BPM


##Controle de prazos

Configuração que define os parâmetros gerais do controle de prazos no ambiente.

###Indicador de Prazo

O indicador de prazo é um "semáforo" que aparece ao lado do prazo dos itens nas listas do GreenDocs.

Na opção "Campo Prazo" define-se o campo de data que será utilizado para controle dos prazos. Depois, a quantidade de dias para que a cor indicativa de prazo seja alterada, quantos dias antes do prazo o indicador deve aparecer em amarelo e quantos dias antes do prazo deve aparecer em vermelho. Itens no prazo aparecerão com o indicador em verde.

Em seguida há a descrição de cada uma das cores de indicação de prazo. A descrição inserida ali é apresentada nos agrupamentos por prazo correspondendo a cada uma das cores.

###Dias ignorados nos cálculos de prazo

Na configuração "Nos cálculos de prazos não considerar", definem-se os dias que não devem ser contados para o cálculo dos prazos: Sábados, domingos ou feriados. Em "Feriados considerados pelo projeto" pode-se adicionar os dias que serão considerados feriados durante o período do projeto, e portando ignorados nos cálculos de prazo.

###Turnos

Os turnos são as horas úteis de trabalho, define-se as horas de início e fim dos turnos do dia. Isto é utilizado nos cálculos de prazo baseados em horas úteis, configurados nos processos/fluxos.


##Funcionalidades

Define alguns dos parâmetros adicionais da configuração do ambiente.

- **Permitir criação de referência entre itens:** Permite que um usuário com acesso de edição aos itens crie uma referência entre dois ou mais itens. O link "Editar Tipos" define os tipos de referência existentes no ambiente a serem utilizados posteriormente na configuração de "Tipos Relacionados" no Tipo de Item.
- **Permitir criação de itens de caminho crítico:** Itens de caminho crítico são marcados em vermelho nas listas do sistema. Com esta opção habilitada, o usuário com acesso poderá marcar que um item é de caminho crítico durante a criação do mesmo, ou quando este estiver em uma etapa de edição e sob sua responsabilidade.
- **Permitir pesquisa por conteúdo:** Habilita a pesquisa por conteúdo de arquivos Office ou PDF no ambiente.
- **Permitir pesquisa por comentário:** Habilita a pesquisa pelo texto dos comentários realizados nos itens do ambiente. 
- **Permite Visualizar Thumbnail:** Habilita a geração de miniaturas para arquivos DWG e Imagens (png, jpg e tif).
- **Exibir botão marcar como não lido:** Permite que o usuário marque um item como não lido.
- **Habilitar auditoria:** Habilita funcionalidade de auditoria no ambiente. Com esta opção marcada, é possível configurar para cada tipo de item critérios de auditoria sobre itens daquele tipo. Usuários que possuírem esta permissão podem então verificar estes critérios em um desenho ou documento diretamente através da interface do GreenDocs na página do item.
- **Utilizar nova interface:** Opção reservada para o suporte do GreenDocs, utilizada em ambientes legado.