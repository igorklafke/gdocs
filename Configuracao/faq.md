#Perguntas Frequentes de Configuração do GreenDocs

###Como habilito para que o usuário possa editar a revisão de um documento?

Na configuração do ambiente, aba Geral -> Revisionamento, marcar a opção "Permitir alteração manual da revisão". E, na aba Permissões -> Usuários, adicionar os usuários desejados na permissão "Permitir alteração manual da revisão".

###Não permitir que um item seja criado se já existir outro item cadastrado com o mesmo valor de um campo.

Na configuração do campo, se este for do tipo "string" marcar a opção "Não permitir valor duplicado no mesmo ambiente".

>ref. [PBI 4115](https://w3k.visualstudio.com/DefaultCollection/GreenDocs%20Git/_workitems?_a=edit&id=4115 "Product Backlog Item 4115: Não permitir duplicação de determinado campo em um form/documento")