#Guia para configuração dos modelos de exportação

##Excel

Na criação do modelo devem ser definidos dois nomes na planilha.

O primeiro indicando o cabeçalho da lista que irá trazer os itens referenciados (apenas a linha com o cabeçalho deve ser selecionada).

![header](http://publicado.greendocs.net.s3.amazonaws.com/docs/img_excel_01.png "Exemplo de configuração do cabeçalho")

Outro uma área de impressão com tamanho dinâmico. Isto é feito utilizando as fórmulas abaixo.

Fórmula para Excel em português:

	=DESLOCAMENTO(Sheet1!$A$1; 0; 10; CONT.VALORES(Sheet1!$A:$A); 1)

Fórmula para Excel em inglês:

	=OFFSET(Sheet1!$A$1, 0, 0, COUNTA($A:$A), 1)
