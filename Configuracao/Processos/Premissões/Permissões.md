#Permissões

Permissões é a área onde devem ser configuradas as permissões de visualização e criação do processo.

##Visualização

Conjunto de permissões que possibilita o usuário apenas visualizar itens sem a possibilidade de edição.

O conjunto de permissões é configurado a partir de uma combinação de resultados de [posições](usuário->posições), sendo eles:

* Unidade
* Área
* Função
* Grupo
* Apenas Concluídos (permiti visualizar apenas itens concluídos)
* Ocultar Comentários (permite ocultar comentários)

##Criação

###Usuários do Processo

Conjunto de permissões que possibilita o usuário a criação de itens no processo.

O conjunto de permissões é configurado a partir de uma combinação de resultadosde [posições](usuário->posições), sendo eles:

* Unidade
* Área
* Função
* Grupo

###Administradores do processo

A permissão de administrador do processo disponibiliza:

* Editar itens sem ser responsável
* Editar informações do usuário
* Editar fluxo do processo
* Receber e-mails de notificação de comentários criados

Pode ser configurada a partir da seleção de usuários e perfis ou através do valor de um metadado.



