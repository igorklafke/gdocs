#Geral

Configuração das informações gerais do processo. 

##Informações Gerais do Processo

A opção "Administradores do processo recebem notificação de comentários", possibilita os administradores do processo receber notificações de comentários em itens do processo.

##Tipos de Item que podem ser criados no processo

Seleção dos tipos de item que devem ser criados utilizando esse processo