#Configuração do Processo

##Fluxo

A configuração do fluxo de trabalho no GreenDocs utiliza a notação BPMN (Business Process Model and Notation), com eventos, atividades e desvios, contectados entre si através de links. Por esta interface gráfica é possível adicionar elementos, clicar e arrastar para desenhar o fluxo e conectar os elementos de maneira fácil. Para adicionar um elemento basta clicar com o botão da direita na posição onde deseja adicioná-lo e escolher o tipo. Também é possível utilizar os botões no cabeçalho da tela para a mesma operação. Para conectar um elemento ao outro, clique com o botão da direita sobre o primeiro elemento e então "Adicionar Link", na sequencia clique sobre o elemento de destino para concluir a criação da conexão.

Clicando duas vezes sobre um elemento, é exibida a tela para configuração das opções do mesmo. Algumas opções são padrão para praticamente todos os tipos de elementos, são elas:

- **Geral:** Configura-se o nome de tela e o nome interno do elemento. Também pode ser configurada uma "Situação" que será preenchida no item quando este passar pelo elemento.
- **Campos:** Indica quais campos do tipo de item associado ao processo serão exibidos para os usuários quando um item estiver nesta etapa do fluxo. Um campo pode ser marcado como de preenchimento obrigatório ou somente leitura. Para adicionar um campo, selecione o mesmo no primeiro combo na parte de baixo da tela e clique em "Adicionar", para adicionar todos os campos de um conjunto de campos de maneira mais rápida, selecione o conjunto no segundo combo e clique em "Adicionar Conjunto".

- **Ações:** Define quais ações irão ser exibidas para o usuário que possuir a responsabilidade pela atividade, as ações aparecem como botões para o usuário na tela do formulário ou documento.

A seguir, são descritos os diferentes tipos de elementos que podem ser utilizados em um fluxo no GreenDocs.

###Eventos

Os eventos são representados por um círculo e indicam algo que *acontece*. Os ícones dentro do círculo indicam o tipo de evento. Eventos também podem ser classificados como de captura (o recebimento de um e-mail ou o vencimento de um prazo, por exemplo) ou de lançamento (como enviar um e-mail ou executar uma ação em outro item relacionado, por exemplo).

Os tipos de evento suportados pelo GreenDocs são:

####Início

Necessário em todos os processos, indica o ínicio do fluxo. Como opções para este elemento, podem ser definidos os campos que irão ser exibidos no momento da criação do documento ou formulário e quais ações irão ser exibidas.


####Início por e-mail

Utilizado para que o processo seja iniciado quando um e-mail for recebido por um [servidor] configurado no GreenDocs. As configurações padrão de Campos e Ações não se aplicam neste evento, mas é necessário selecionar o servidor de e-mail na aba geral das opções e quais campos no GreenDocs receberão os valores dos atributos do e-mail "De", "Para", "Assunto" e "Corpo". Além disso há a opção de "Cadastrar os anexos do email como arquivos" e "Referenciar à item existente através de tag no assunto". Com esta última opção marcada, o GreenDocs, ao receber um e-mail no servidor configurado e criar o novo item, irá procurar no assunto do e-mail uma tag indicada pelo caractere #. Com esta tag, tentará localizar um outro item já exisitente no GreenDocs cujo nome seja igual ao valor da tag, encontrando um, uma referência será criada entre o novo item e o item com o nome da tag.

####Notificação

Quando um item passa por este evento uma notificação por e-mail é enviada. Nas opções de configuração, é necessário selecionar o [modelo de e-mail] que será utilizado e qual o tipo de notificação, que indica para quem a mensagem deverá ser enviada:

- Requisitante: O email será enviado para o requisitante, pessoa que iniciou o processo.
- Valores de campos: O email será enviado para os usuários que estiverem preenchidos nos [campos do tipo lista de usuários] adicionados.
- Posições: Email será enviado para os usuários que possuírem as [posições] adicionadas.
- Processos irmãos: O email será enviado para os reponsáveis por "processos irmãos" do processo atual, ou seja, itens que estejam referenciados como filhos de um item que esteja referenciado como pai do item atual.
- Último responsável: Usuário que foi o responsável pela última atividade executada no fluxo receberá a notificação.

As configurações padrão de Campos e Ações não se aplicam neste evento.
 
####Sinal
 
Emite um "sinal" para itens referenciados ao processo atual. Este sinal fará que uma atividade de fluxo seja executada automaticamente sobre os itens filhos do item atual. Na configuração geral do elemento, deve-se selecionar o tipo de item relacionado onde a ação será executada, a atividade e a ação. A medida que uma destas opções vai sendo selecionada, o valor da próxima é atualizado listando apenas as opções possíveis. A caixa de seleção "Executar a ação somente se o item relacionado estiver na atividade correspondente a ação" deve ficar desmarcada se for necessário que a ação seja executada independentemente da atividade atual dos itens filhos. 

Existe a opção de inserir regras para que apenas itens relacionados que corresponderem as regras executem a ação, estas podem ser configuradas utilizando metadados do tipo "lista" e "checkbox" e um operador lógico para escolher "igual" ou "diferente de".

As configurações padrão de Campos e Ações não se aplicam neste evento.

####Revisão

Gera uma nova revisão para o item. Se o ambiente estiver configurado para [revisionamento composto], deve-se selecionar também se a revisão cheia ou parcial deverá ser incrementada.

A opção "Gerar sub revisão" irá criar uma subrevisão para o item atual, esta sub-revisão será finalizada no próximo evento de revisão que não tenha essa opção também marcada pelo qual o item passar.

As configurações padrão de Campos e Ações não se aplicam neste evento.

####Treinamento

Cria uma pendência de treinamento sobre o documento. Nas configurações gerais, selecionar o [modelo de e-mail] que será utilizado na notificação para os usuários que precisem se treinar. A definição de quais usuários deverão ser treinados sobre quais documentos é realizada na interface de usuário pelas pessoas que possuírem a [permissão adequada].

As configurações padrão de Campos e Ações não se aplicam neste evento.

####Cópia de campos

Atualiza em um item referenciado pai os campos que ambos possuírem em comum, copiando a partir dos valores do item atual quando este passa pelo evento.

As configurações padrão de Campos e Ações não se aplicam neste evento.

####Compartilhar

Utilizado para compartilhamento da revisão atual do documento com determinados usuários, dando a eles acesso de leitura ao documento. Nas configurações gerais do evento deve ser selecionado o [modelo de e-mail] que será utilizado na notificação dos usuários com os quais o documento for compartilhado.

Na aba "Responsáveis" devem ser selecionados os usuários com os quais o item será compartilhado, podem ser:

- Usuários selecionados: Simplesmente irá compartilhar com os usuários selecionados aqui.
- Valores de campos: O documento será compartilhado com os usuários que estiverem preenchidos nos [campos do tipo lista de usuários] adicionados.
- Posições: O documento será compartilhado com os usuários que possuírem as [posições] adicionadas.

####Fim Parcial

Finaliza a ramificação atual do fluxo, não interferindo em outras possíveis ramificações.

####Fim Total

Finaliza totalmente o fluxo, na ramificação atual e todas as outras que possivelmente estiverem em andamento.


###Atividades

As atividades são representadas por um retângulo e indicam algo que precisa ser *feito*. Uma atividade pode ser uma atividade simples, uma atividade múltipla em paralelo ou um sub-processo.

Uma atividade simples ou múltipla possuem as mesmas opções, porém a atividade múltipla irá gerar uma instância independente para cada responsável e o fluxo só irá continuar quando todos os responsáveis executarem uma ação. Já na atividade simples, o processo continua quando qualquer um dos responsáveis executa alguma ação.

Para configurações específicas do sub-processo ver a seção correspondente abaixo.

Para as atividades simples ou múltiplas, além das opções padrão, algumas configurações adicionais são possíveis para este tipo de elemento:

####Geral

- Ícone: Uma imagem que será exibida na listagem de documentos para o usuário quando o item estiver nesta atividade.
- Descrição: Um texto breve com instruções para execução da atividade que é exibido no topo da tela para o usuário quando este acessar um formulário que encontra-se nesta atividade.
- Atividade fim de fluxo: Caixa de seleção que quando marcada, fará com o que o documento não apareça como pendência para o usuário responsável. É utilizada geralmente quando a atividade indica um documento liberado, permitindo que o usuário responsável inicie um novo fluxo (revisão) sem sobrecarregar a lista de pendências do mesmo com documentos já liberados.
- Exigir anexo em comentário: Caixa de seleção que quando marcada fará que comentários executados sobre itens nesta atividade necessitem obrigatoriamente de um arquivo anexado.

####Arquivos

Opções relacionadas aos arquivos anexados ao formulário ou documento.

- Permite Incluir Arquivo: Define se o usuário responsável pode incluir arquivos novos no item quando este estiver nesta atividade.
- Permite Atualizar Arquivo: Define se o usuário responsável pode substituir um arquivo existente por um novo. Além das opções "Permitido" e "Não permitido", há a opção "Permitido para quem incluiu o arquivo" que permite a substituição do arquivo apenas se este tiver sido incluído pelo usuário atual.
- Permite Excluir Arquivo: Define se o usuário responsável pode remover um arquivo existente. As opções possíveis são as mesmas que para atualização do arquivo.
- Permitir Arquivos de Rascunho: Caixa de seleção que, se marcada, permite que o usuário inclua vários arquivos de rascunho que serão posteriormente automaticamente descartados quando o documento sair desta atividade. Quando esta opção está habilitada, o usuário precisa marcar na lista de arquivos do documento aqueles que ele deseja manter antes de concluir a atividade. O sistema irá obrigar que pelo menos um arquivo esteja selecionado antes de dar continuidade ao fluxo.
    
####Ações

As ações de uma atividade podem ter regras que irão definir seu comportamento. Ao clicar no link "Regras" junto ao nome de uma ação, uma nova tela é exibida com as seguintes opções:

- Exigir comentário: Torna o preenchimento de um comentário obrigatório quando o usuário executar esta ação.
- Resultados da Ação: Altera a [Situação] da revisão atual do documento para o valor selecionado na lista.
- Os campos abaixo devem ser preenchidos para permitir a execução desta ação: Define quais campos da atividade atual devem ser obrigatoriamente preenchidos pelo usuário antes que ele possa executar esta ação.
- Exigir inclusão/atualização de arquivo: Faz com que o usuário precise incluir um arquivo ou atualizar o existente antes de poder executar a ação.
- A ação será visível apenas para usuários da posição abaixo: Define-se as [posições] para os quais a ação será exibida. Se nenhuma posição for selecionada, a ação será exibida para todos os responsáveis da atividade.
    
####Responsáveis

Define os usuários que serão responsáveis pela atividade. As opções disponíveis são:

- Responsabilidade Randomizada: Caixa de seleção que quando marcada, irá fazer com o que o responsável pela atividade seja selecionado randomicamente dentre os usuários definidos como possíveis responsáveis na opção "Definido por".
- Modelo de email notificação: O [modelo de e-mail] que será utilizado na notificação para os responsáveis pela atividade. Se nenhum modelo for selecionado, o modelo padrão de responsabilidade será enviado.
- Definido por: Indica como os responsáveis pela atividade serão selecionados:
    - Usuários selecionados: Os responsáveis pela atividade serão os usuários selecionados aqui.
    - Valores de campos: Os responsáveis serão os usuários que estiverem preenchidos nos [campos do tipo lista de usuários] adicionados.
    - Posições: Os responsáveis pela atividade serão os usuários que possuírem as [posições] adicionadas.
    - Criador: O responsável será o usuário que criou o documento ou iniciou o fluxo do item.
    - Usuário logado: O responsável será o usuário que estiver logado atualmente, ou seja, o usuário que estiver executando a ação que fez o item chegar nesta atividade.
    
####Campos

As opções para os campos exibidos na atividade são as opções padrão já descritas na definição do Fluxo no início deste capítulo.

####Prazo

Configura-se como o prazo para a execução da atividade será definido:

- Sem prazo: A atividade não terá um prazo para execuçao.
- Fixo em horas: Quantidade de horas contando a partir do momento em que o item entra na atividade. A opção "Apenas horas úteis" irá utilizar a configuração de [turnos] do ambiente para ignorar na contagem do prazo horas que não façam parte da jornada de trabalho.
- Valor de um campo data/hora: O prazo será o valor preenchido em um campo do tipo data ou data e hora do item.
- Valor de um campo em dias: O prazo será a quantidade de dias que estiver preenchida em um campo do tipo inteiro do item. A opção "Prazo não pode ser fim de semana" quando marcada, irá deslocar o prazo para o próximo dia útil se este cair em um final de semana.

Para todas as possibilidades de prazo, há uma opção "Executar ação automaticamente ao terminar prazo". Ao marcar esta opção, deve-se selecionar uma das ações da atividade para ser executada automaticamente, a ação selecionada aqui não será visível para o usuário como um botão. Com esta opção, um evento do tipo tempo é anexado à atividade. Este evento pode então ser conectado à outro elemento através de um link, assim quando o prazo vencer o item poderá andar automaticamente no fluxo.

###Sub-Processos

Um subprocesso é um tipo de atividade, as atividades deste tipo possuem uma aba "Sub Prcesso" na tela de configuração com as seguintes opções:

- Assíncrono: Com esta opção marcada o fluxo principal não irá aguardar a conclusão do sub-processo para continuar sua execução, passando diretamente para o próximo elemento após iniciar o sub-processo.
- Sub-Processo: O processo que define o sub-processo a ser criado.
- Disparar múltiplas instâncias conforme linhas de tabela multivalorada: Quando um [campo do tipo tabela] for selecionado aqui, uma instância do sub-processo será criada para cada linha da tabela e as colunas da tabela serão copiadas como campos no sub-processo. Quando todos os sub-processos forem concluídos, as linhas da tabela no processo principal são atualizadas com os valores dos campos do sub-processo.
- O sub-processo não será criado quando as regras abaixo forem atendidas: São regras de exceção para a criação do sub-processo, quando o item possuir os valores dos campos como configurado aqui, o sub-processo não será iniciado.

###Eventos Anexos a Atividades

Um evento é anexado a uma atividade quando esta necessitar de uma saída alternativa baseada em eventos externos. Após anexar um evento na atividade, este pode ser conectado à outro elemento através de um link.

Para anexar um evento a uma atividade, clique com o botão da direita sobre a mesma, e selecione a opção "Anexar Evento". Dois tipos de eventos podem ser anexados as atividades:

####Condicional

É a reação a alterações nas condições de negócio ou regras de negócio. O evento "escuta" por determinadas condições e tem sua saída executada quando esta é atendida, suas opções são:

- Nome: Para ser exibido na tela de configuração.
- Quando: Condição que irá disparar o evento:
    - Todos os itens relacionados estiverem em uma atividade: Configura-se o tipo de item e a atividade, quando todos os itens deste tipo que estiverem relacionados ao tipo atual entrarem na atividade configurada, o evento condicional será executado.
    - Todos os itens relacionados estiverem em um evento final: Configura-se o tipo de item e o evento, quando todos os itens deste tipo que estiverem relacionados ao tipo atual chegarem ao evento final configurado, o evento condicional será executado.
    - Todos os itens descendentes atenderem a regra: Configura-se o tipo de item, a atividade e os campos e seus valores, quando todos os itens deste tipo que estiverem relacionados ao tipo atual ou a itens filhos do item atual entrarem na atividade configurada e possuírem os valores dos campos conforme configurado na regra, o evento condicional será executado.
- Ação: Selecionar uma das ações da atividade para ser executada automaticamente, a ação selecionada aqui não será visível para o usuário como um botão.
- Posição: Indica graficamente o local onde o evento será anexado na atividade, 0 para canto inferior direito, 1 para canto inferior esquerdo, 2 para canto superior esquerdo ou 3 para canto superior direito.


####Temporal

Indica um instante no tempo ou intervalo de tempo quando a saída do evento deve ser executada, suas opções são:

- Nome: Para ser exibido na tela de configuração.
- Definido por: A regra que define a data ou intervalo em que a ação de saída do evento deve ser executada.
    - Fixo em dias: Quantidade de dias após o início da atividade quando a ação deve ser executada. Preencher o campo "Dias".
    - Fixo em horas: Quantidade de hotas após o início da atividade quando a ação deve ser executada. Preencher o campo "Horas".
    - Valor de um campo data/hora: O evento será executado na data preenchida em um campo do tipo data ou data e hora do item. Selecionar o campo.
    - Valor de um campo em dias: O evento será executado após a quantidade de dias preenchida em um campo do tipo inteiro no item. Selecionar o campo.
- Não interromper a execução da instância: Quando marcada, a saída do evento é executada mas a instância que se encontra na atividade atual não é alterada, consequentemente, se o evento estiver definido por uma quantidade de dias ou horas esta saída pode ser executada mais de uma vez enquando esta atividade não for concluída, por exemplo, "a cada 30 dias". Se a opção estiver desmarcada, a instância da atividade atual é concluída. Pode ser diferenciado no desenho do fluxo, pois um evento que não interrompe a execução da instância parace com a borda pontilhada.
- Ação: Selecionar uma das ações da atividade para ser executada automaticamente, a ação selecionada aqui não será visível para o usuário como um botão.
- Posição: Indica graficamente o local onde o evento será anexado na atividade, 0 para canto inferior direito, 1 para canto inferior esquerdo, 2 para canto superior esquerdo ou 3 para canto superior direito.


###Desvios

Os desvios são representados por um diamante e determinam uma bifurcação ou fusão de caminhos do fluxo, dependendo das condições expressas. Na configuração das opções do desvio será determinado o seu tipo:

####Desvio Condicional Exclusivo

Em um ponto de ramificação, seleciona exatamente um caminho de saída dentre as alternativas existentes. Em um ponto de convergência, basta a execução completa de um braço de entrada para que seja ativado o fluxo de saída.

Ao selecionar este tipo de desvio, para cada possível saída podem ser adicionadas regras que devem ser atendidas para que a saída seja executada. Cada regra pode ser a última ação de fluxo executada ou o valor de um campo. Se a regra for a última ação executada, deve-se selecionar qual a ação. Se for um campo, seleciona-se um operador (é igual a, é diferente de, está preenchido ou não está preenchido) e o valor se for o caso.

A primeira saída cuja regra for atendida será executada.

####Ativação Incondicional em Paralelo

Em um ponto de ramificação, todos os fluxos de saída são ativados simultaneamente. Em um ponto de convergência de fluxos, espera que todos os caminhos de entrada completem, antes de disparar o fluxo de saída.

Não é necessário configurar nenhuma regra para este tipo de desvio.

####Ativação Inclusiva Condicional

É um ponto de ramificação, após avaliar condições, um ou mais caminhos são ativados. Em um ponto de convergência de fluxos, espera que todos os fluxos de entrada ativos tenham completado para ativar o fluxo de saída.

Ao selecionar este tipo de desvio, para cada possível saída podem ser adicionadas regras que devem ser atendidas para que a saída seja executada. Cada regra pode ser a última ação de fluxo executada ou o valor de um campo. Se a regra for a última ação executada, deve-se selecionar qual a ação. Se for um campo, seleciona-se um operador (é igual a, é diferente de, está preenchido ou não está preenchido) e o valor se for o caso.

Todas as saídas que atenderem a regra configurada para ela serão executadas.