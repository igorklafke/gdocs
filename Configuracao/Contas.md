#Configuração de Contas - Geral
Possibilita cadastro de informações gerais, cadastro de usuários e cadastro de grupos na conta.
## Informações Gerais da Conta
Nessa sessão é possível informar e configurar as seguintes opções:

- Nome
- Número (identificador numérico da conta)
- Quantidade máxima de usuários
- Quantidade atual de usuários
- Grupo dono (existente ou novo)
- Bloquear edição de dados pelos usuários
- Usar autenticação via AD
- Escolher usuários administradores

###Quantidade máxima de usuários

Quantidade máxima de usuários que todas os ambientes da conta irão compartilhar


###Quantidade atual de usuários

Quantidade atual de usuários dos ambientes da conta somados


###Bloquear edição de dados pelos usuários
Impede que o usuário altera seus dados pessoais na tela "Meus dados".


###Usar autenticação via AD
Faz a autenticação de usuários via Active Directory


###Escolher usuários administradores
Permite que sejam selecionados usuários que podem administrar a conta.


##Cadastro de usuários
Possibilita o cadastro de campos para informações adicionais de usuário. 
Essas informações servem por exemplo, na exportação de um formulário para um Word, se o formulário tiver um campo do tipo lista de usuários, pode ser configurado na exportação para que essas informações sejam exportadas no Word.


##Cadastro de grupos
Possibilita o cadastro de campos para informações adicionais de grupo. 
Essas informações servem por exemplo, na exportação de um formulário para um Word, se o formulário tiver um campo do tipo lista de grupos, pode ser configurado na exportação para que essas informações sejam exportadas no Word.
##Grupos e Usuários / Perfis
Permite cadastrar, editar e excluir/desativar grupos, usuários e perfis de usuários.
###Grupos
####Geral
Permite informar o nome do grupo e inserir um logotipo (Imagem)
####Permissões do grupo
Lista quais projetos o grupo é dono.
Permite informar em quais projetos o grupo é contratante.
Permite informar em quais projetos o grupo é participante.
Lista em quais processos o grupo é Dono.
Permite informar em quais processos o grupo é participante.
####Informações adicionais
Permite que as informações adicionais do grupo sejam informadas e salvas.
####Excluir / Desativar
Permite excluir / desativar um grupo ativo no sistema, desde que o mesmo não participe de um projeto ou processo ou que tenha usuários cadastrados. Se por algum desses motivos o grupo não puder ser excluído / desativado o usuários será informado disto.
####Grupos Desativados
Possibilita a reativação de grupos que foram desativados anteriormente

----------

###Usuários
Após um grupo ser cadastrado o mesmo pode receber o cadastro de usuários e perfis.
####Novo Usuário - Aba Geral
Permite alterar os dados do usuário, tais como: Nome, e-mail, e-mail para notificações, idioma, perfis do usuários, usuário substituto, marcar usuário como ausente, marcar como contato, definir senha de acesso.
#####E-mail
E-mail que o usuário irá usar para fazer login no Greendocs
#####Idioma
Idioma que o Greendocs irá assumir quando o usuário realizar o login no Greendocs. O idioma configurado para o usuário se sobrepõem ao configurado para o ambiente.
#####Perfis
Atribuição de perfis previamente cadastrados ao usuário. Um usuário pode participar de vários perfis ou nenhum.
#####Usuário substituto
Usuário que ficará responsável pelas pendências do usuário (titular) quando o mesmo estiver ausente.
#####Ausente
Deve ser marcado quando o usuário ficar ausente das atividades. Ao marcar essa opção o usuário substituto ficará responsável pelas pendências ligadas ao usuário (titular).
#####Senha
Define a senha de acesso ao Greendocs
#####Confirmar senha
Confirmação da senha escolhida para acesso ao Greendocs
#####Usuário deve trocar de senha no próximo login
Obriga o usuário a alterar a senha de acesso para ter acesso ao Greendocs
#####Imagem do usuário
Imagem / foto de identificação visual do usuário.
####Manutenção de usuário (Edição) - Aba Geral
Permite alterar os mesmo dados da inclusão e mais opções descritas a seguir.
#####Redefinir senha
Deve-se informar a nova senha e confirmar a mesma. Marcando a opção *Usuário deve trocar de senha no próximo login* o usuário será obrigado a alterar a senha de acesso ao Greendocs ao tentar efetuar o login novamente.
#####APIKey
Chave única que serve para acesso ao Greendocs via API.
#####Desabilitar Usuário
Possibilita que um usuário ativo do Greendocs seja desabilitado e com isso não terá mais acesso ao Greendocs. Um usuário desabilitado pode ser reabilitado posteriormente.
#####Acessar o Greendocs com este usuário
Permite que seja efetuado o login sem nescessidade de informar usuário e senha a partir dessa tela.
####Manutenção de usuário (Edição) - Aba Permissões do usuário
Permite a edição das permissões de projetos para o usuário. Para cada permissão pode-se adicionar vários projetos ou nenhum.
####Posições do usuário
Lista os projetos que pertencem ao grupo do usuário.
Ao clicar em um destes projetos, pode-se adicionar posições de Unidade, Área e Função.
####Informações Adicionais
Permite que as informações adicionais do usuário sejam informadas e salvas.
###Usuários desativados
Possibilita a reativação de grupos que foram desativados anteriormente.
###Perfis
Um perfil pode ser atribuído a vários usuários. Os usuários que fazem parte do perfil  partilham das responsabilidades e permissões concedidas ao perfil.
####Novo Perfil
Define o nome do perfil e quais usuários farão parte do perfil.
####Perfis Desativados
Possibilita a reativação de perfis que foram desativados anteriormente

----------
##Informações Gerenciais
###Informações Gerais de Uso
Apresenta uma tabela com os projetos da conta e número de itens criados, revisões e indexamentos de cada projeto da conta.
###Log de Envio de E-mails
Apresenta uma tabela com os registros por dia de envios ou tentativas de envios de e-mails. Informa o dia, quantidade de e-mails enviados, quantidade de e-mails não enviados e erros.
####Abrir log detalhado (e-mails)
Relatório de log detalhado de e-mails. Permite filtrar por conta, etapa origem, destinatário, últimos 30,60 ou 90 dias, projeto, etapa destino e nome do documento.
####Relatório de Atividades
Relatório de atividades de itens na conta. Permite filtrar por Data inicial, data final, evento, conta, projeto, usuário e documento.