#Configurações de Instância
##Informações de Licenças
Permite que uma licença seja adicionada, editada ou excluída.
Apresenta uma tabela com as licenças já adicionadas, com código do cliente, quantidade de usuários, data da ativação, data de validade.

As licenças já adicionadas podem ser editadas ou excluídas.

###Adicionar / Editar licença
Ao adicionar ou editar uma licença deve-se informar o *Nome do Cliente*, *CPNJ*, *Quantidade de usuários regulares* e *Quantidade de usuários de leitura*.

Ao clicar no botão *Solicitar Registro* o usuário será direcionado para a tela de Ativação de Licença.

Para concluir a ativação, deve-se fazer o download do arquivo de registro através do link *Baixar arquivo de registro*,e enviá-lo à W3K solicitando a ativação.

Após enviar o arquivo de registro para a W3K, a solicitação será processada e, se as informações do registro estiverem de acordo com o contrato, um arquivo de ativação será enviador pela W3K ao solicitante da ativação. Esse arquivo deve ser aplicado no GreenDocs para habilitar o uso.

##Ferramentas
###Relatório de Atividades
Relatório de atividades de itens na conta. Permite filtrar por Data inicial, data final, evento, conta, projeto, usuário e documento.
###Configurações Avançadas
Permite a configuração de parâmetros internos para geração de links e visualizador, através dos campos *URL Servidor Local* e *URL Servidor Publico*. A caixa de seleção *Forçar utilização de HTTPS* obriga que os links sejam HTTPS.

E-mail para envio será o e-mail utilizado para enviar os e-mails que o Greendos envia internamente, notificações, responsabilidades, etc.

Pasta raiz é a pasta raiz em que os arquivos serão salvos no servidor.

####Método Envio de E-mails
#####SMTP
Servidor SMTP é o servidor que será usado para enviar os e-mails internos do Greendocs.
Nome usuário SMTP é o usuário que tem acesso ao servidor SMTP informado anteriormente.
Senha do usuário com acesso ao servidor SMTP e porta do servidor
A caixa de seleção *Utilizar TLS* deve ser marcada quando o servidor SMTP utilizar esse tipo de autenticação.
#####Mail GUN
Uso exclusivo de nuvens publicas.
###Referência API
Link com ajuda da API do Greendocs
##Informações Gerais de Uso
Apresenta uma tabela com uma linha para cada conta da instância.
As colunas são compostas por, quantidade de projetos, quantidade de grupos, quantidade de usuários, quantidade de itens, quantidade de revisões e quantidade de indexados.
##Log de Envio de E-mails
Apresenta uma tabela com os registros por dia de envios ou tentativas de envios de e-mails. Informa o dia, quantidade de e-mails enviados, quantidade de e-mails não enviados e erros.
###Abrir log detalhado (e-mails)
Relatório de log detalhado de e-mails. Permite filtrar por conta, etapa origem, destinatário, últimos 30,60 ou 90 dias, projeto, etapa destino e nome do documento. 
##Estatísticas
Apresenta uma tabela de estatísticas que pode ser por:

- Dia: dia, page views e visitas
- Projeto: projeto, page views e visitas
- Cidade: cidade, page views e visitas
- Navegador: cidade, page views e visitas

##Log dos Últimos 5 Erros
Apresenta uma tabela com os registros por ID, Data, Origem, Método e Linha dos últimos 5 erros gerados.
###Abrir log de erros completo
Relatório de log de erros. Permite filtrar por data, projeto, usuário, resolvidos, não resolvidos ou todos, origem, método e navegador.