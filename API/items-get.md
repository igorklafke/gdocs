# GET api/v2/items

Retorna os dados do documento ou formulário

## Request

### Parâmetros

| Nome          | Descrição                                                          | Tipo   |
|---------------|------------------------------------------------------------------- |--------|
| NomeDocumento | Nome do documento para fazer o download do arquivo | string |
| TipoItem      | ID do Tipo de Item do documento para fazer o download | long |


## Response

String JSON de um dicionário chave/valor onde a chave é o nome interno do campo no GreenDocs e o valor é o que está preenchido no campo.

Exemplo: 

     {"Nome_Campo":"ValorCampo1","Nome_Campo2":"ValorCampo2"}