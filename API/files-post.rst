=================
POST api/v2/files
=================

Adiciona um arquivo no item

Request
=======

Parâmetros
----------

==================== ===================================================================== ========
Nome                 Descrição                                                             Tipo
==================== ===================================================================== ========
NomeDocumento        Nome do documento em que o arquivo será criado                        string
TipoItem             ID do Tipo de Item do documento em que o arquivo será criado          long
RevisaoDocumento     (opcional) Número da revisão em que o arquivo será inserido,          string 
                     se não preenchido, insere na revisão atual   
*aquivo*             O arquivo deve ser enviado no corpo da requisição                     Campos
==================== ===================================================================== ========

Response
========

HTTP status code 200 se a inserção for bem sucedida.