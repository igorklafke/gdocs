# PUT api/v2/items

Atualiza os dados de um item existente

## Request

### Parâmetros

| Nome          | Descrição                                                          | Tipo   |
|---------------|------------------------------------------------------------------- |--------|
| NomeDocumento | Nome do documento que será atualizado | string |
| TipoItem      | ID do Tipo de Item do documento a ser atualizado | long |
| Usuario       | Login do usuário ao qual a atualização do documento ficará relacionada | string |
| Campos        | Campos a serem atualizados | Campos |
| RevisaoDocumento | Novo número de revisão para o documento | string |
| GerarRevisao | (opcional) Se preenchido irá criar uma nova revisão do documento antes de atualizar | string |

### Tipos específicos

#### Campos

Dicionário chave/valor onde a chave é o nome interno do campo no GreenDocs e o valor é o que será preenchido no campo. 

## Response

HTTP status code 200 se a criação for bem sucedida.