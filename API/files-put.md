# PUT api/v2/files

Substitui o arquivo no item

## Request

### Parâmetros

| Nome          | Descrição                                                          | Tipo   |
|---------------|------------------------------------------------------------------- |--------|
| NomeDocumento | Nome do documento em que o arquivo será atualizado | string |
| TipoItem      | ID do Tipo de Item do documento em que o arquivo será atualizado | long |
| RevisaoDocumento       | (opcional) Número da revisão em que o arquivo será inserido, se não preenchido, insere na revisão atual | string |
| *aquivo*        | O arquivo deve ser enviado no corpo da requisição | Campos |


## Response

HTTP status code 200 se a atualização for bem sucedida.