# GET api/v2/files

Faz o download do arquivo.

## Request

### Parâmetros

| Nome          | Descrição                                                          | Tipo   |
|---------------|------------------------------------------------------------------- |--------|
| NomeDocumento | Nome do documento para fazer o download do arquivo | string |
| TipoItem      | ID do Tipo de Item do documento para fazer o download | long |


## Response

Download do arquivo.