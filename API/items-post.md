# POST api/v2/items

Cria um novo item

## Request

### Parâmetros

| Nome          | Descrição                                                          | Tipo   |
|---------------|------------------------------------------------------------------- |--------|
| NomeDocumento | Nome do documento que será criado | string |
| TipoItem      | ID do Tipo de Item do documento a ser criado | long |
| Usuario       | Login do usuário ao qual a criação do documento ficará relacionada | string |
| Campos        | Campos para cadastro do documento | Campos |


### Tipos específicos

#### Campos

Dicionário chave/valor onde a chave é o nome interno do campo no GreenDocs e o valor é o que será preenchido no campo.

Exemplo: 

     {"Nome_Campo":"ValorCampo1","Nome_Campo2":"ValorCampo2"}

## Response

HTTP status code 200 se a criação for bem sucedida.